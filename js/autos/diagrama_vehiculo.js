//Cargamos la imagen para daños
var canvas_3 = document.getElementById('canvas_3');
var ctx_3 = canvas_3.getContext("2d");
var background = new Image();

if ($("#danosMarcas").val() == "") {
    background.src = $("#direccionFondo").val();
} else {
    background.src = $("#danosMarcas").val();
}

background.onload = function(){
    ctx_3.drawImage(background,0,0,400,300);
}

$("#canvas_3").click(function(e){
    getPosition(e);
});

// var pointSize = 3;
function getPosition(event){
    var rect = canvas_3.getBoundingClientRect();
    var x = event.clientX - rect.left;
    var y = event.clientY - rect.top;
    //Recuperamos el tipo de marca que se pondra
    var marca = $("input[name='marcasRadio']:checked").val();
    //Verificamos que se haya marcado la opcion de pitar
    var indicador_danos = $("input[name='danos']:checked").val();
    if (indicador_danos == "1") {
        switch(marca)
        {
            case 'hit':
                draw(ctx_3, x, y, 8);
            break;
            case 'broken':
                drawX(ctx_3,x, y, 3);
            break;
            case 'scratch':
                drawZ(ctx_3,x, y, 5);
            break;
            default:
            
            break;
        }
    }        
    // drawCoordinates(x,y);
}

//Funciones de dibujo para diagrama de daños
function draw(ctx,x,y,size) {
    ctx.fillStyle = "#ffffff"
    ctx.strokeStyle = "#ff0000"
    ctx.beginPath()
    ctx.arc(x, y, size, 0, Math.PI*2, true)
    ctx.closePath()
    ctx.fill()
    ctx.stroke()
}

function drawX(ctx,x,y,size){
    ctx.beginPath();
    ctx.strokeStyle = "#00ff00";
    ctx.moveTo(x - 15, y - 15);
    ctx.lineTo(x + 15, y + 15);

    ctx.moveTo(x + 15, y - 15);
    ctx.lineTo(x - 15, y + 15);
    ctx.stroke();
}

function drawZ(ctx,x,y,size){
    // ctx.font = "15px Arial";
    ctx.fillStyle = "#0000ff";
    ctx.font = "20px Arial";
    ctx.fillText("#", x, y);
}

function draw(ctx,x,y,size) {
    ctx.fillStyle = "#ffffff"
    ctx.strokeStyle = "#ff0000"
    ctx.beginPath()
    ctx.arc(x, y, size, 0, Math.PI*2, true)
    ctx.closePath()
    ctx.fill()
    ctx.stroke()
}

//Orden de servicio descarga de imagenes
/*$('#btn-download').on('click',function(){
    if ($("#danosMarcas").val() == "") {
        var button = document.getElementById('btn-download');
        button.addEventListener('click', function (e) {
            var dataURL = canvas_3.toDataURL('image/png');
            button.href = dataURL;
        });
    }
});*/

var button = document.getElementById('btn-download');
button.addEventListener('click', function (e) {
    if ($("#danosMarcas").val() == "") {
        var dataURL = canvas_3.toDataURL('image/png');
        button.href = dataURL;
    }
    var dataURL = canvas_3.toDataURL('image/png');
    button.href = dataURL;
});

//Presionamos el boton para resetear la imagen (diagrama)
$("#resetoeDiagrama").on('click',function(){
    background.src = $("#direccionFondo").val();
}); 

function activar_marcas() {
    // recuperamos el valor
    var indicador_danos = $("input[name='danos']:checked").val();

    //si el indicador es 0 bloqueamos la edidcon
    if (indicador_danos == "0") {
        $("input[name='marcasRadio']").attr('disabled', true);
    }else{
        $("input[name='marcasRadio']").attr('disabled', false);
    }
}