function inicializaTabla() {
	var tabla_productos = $('#listadoPolizaDevolucion').DataTable({
		language: {
			url: PATH_LANGUAGE
		},
		"ajax": {
			url: PATH_API + "api/polizas/devolucion_ventas/listado",
			type: 'POST',
			data: {
				fecha_inicio: $("#fecha_inicio").val(),
				fecha_fin: $("#fecha_fin").val()
			}
		},
		columns: [
			{
				title: "folio",
				data: 'folio',
			},
			{
				title: "Clave<br>producto",
				data: 'no_identificacion',
			},
			{
				title: "Descrición",
				data: 'descripcion',
			},
			{
				title: "Cantidad",
				data: 'cantidad'	
			},
			{
				title: "V/Unitario",
				data: 'valor_unitario',
			},
			{
				title: "Total/Producto",
				data: 'venta_total_producto',
			},
			{
				title: "Observaciones",
				data: 'observaciones',
			},
			{
				title: "Total devolución",
				data: 'total_devolucion',
			},
			{
				title: "Estatus",
				data: 'estatusVenta',
			},
		]
	});
}

function getTotalesVentas(){
	ajax.post(`api/polizas/devolucion_ventas/totales`, {
		fecha_inicio: $("#fecha_inicio").val(),
		fecha_fin: $("#fecha_fin").val()
	}, function(response, headers) {
		if (headers.status == 200) {
			$("#totalVentas").html(utils.isDefined(response.total_ventas) ? response.total_ventas : '');
			$("#totalCantidad").html(utils.isDefined(response.total_cantidad) ? response.total_cantidad : '');
			$("#sumaVUnitario").html(utils.isDefined(response.sum_valor_unitario) ? response.sum_valor_unitario : '');
			$("#sumaTotal").html(utils.isDefined(response.sum_venta_total) ? response.sum_venta_total : '');
		}
	});
}

this.inicializaTabla();
this.getTotalesVentas();