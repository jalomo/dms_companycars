function inicializaTabla() {
	var tabla_productos = $('#listadoPolizaCompras').DataTable({
		language: {
			url: PATH_LANGUAGE
		},
		"ajax": {
			url: PATH_API + "api/polizas/compras/listado",
			type: 'POST',
			data: {
				fecha_inicio: $("#fecha_inicio").val(),
				fecha_fin: $("#fecha_fin").val()
			}
		},
		columns: [{
				title: "#",
				data: 'id',
			},
			{
				title: "Clave<br>producto",
				data: 'no_identificacion',
			},
			{
				title: "Descrición",
				data: 'descripcion',
			},
			{
				title: "Unidad",
				data: 'unidad',
			},
			{
				title: "Cantidad",
				render: function(data, type, row) {
					if (row.estatusId == 2 || row.estatusId == 3){
						return '<span class="text-success text-center">' + row.cantidad + '</span>';
					} else {
						return '<span class="text-danger text-center">' + row.cantidad + '</span>';
					}
				}
			},
			{
				title: "V/Unitario",
				data: 'precio',
			},
			{
				title: "Estatus",
				data: 'estatusCompra',
			},
			{
				title: "Total",
				data: 'total',
			},
		]
	});
}

function getTotalesCompras(){
	ajax.post(`api/polizas/compras/totales`, {
		fecha_inicio: $("#fecha_inicio").val(),
		fecha_fin: $("#fecha_fin").val()
	}, function(response, headers) {
		if (headers.status == 200) {
			$("#totalOrdenes").html(utils.isDefined(response.total_compras) ? response.total_compras : '');
			$("#totalCantidad").html(utils.isDefined(response.total_cantidad) ? response.total_cantidad : '');
			$("#sumaVUnitario").html(utils.isDefined(response.sum_valor_unitario) ? response.sum_valor_unitario : '');
			$("#sumaTotal").html(utils.isDefined(response.sum_compra_total) ? response.sum_compra_total : '');
		}
	});
}

this.inicializaTabla();
this.getTotalesCompras();