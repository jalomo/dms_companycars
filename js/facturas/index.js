$(document).ready(function () {
    $("#subirfactura").on('click', function (e) {
        var paqueteDeDatos = new FormData();
        paqueteDeDatos.append('archivo_factura', $('#archivo_factura')[0].files[0]);
        ajax.postFile(`api/productos/upload-factura-productos`, paqueteDeDatos, function (response, header) {
            if (header.status == 200) {
                let titulo = "Factura subida con exito!"
                utils.displayWarningDialog(titulo, 'success', function (result) {
                    return window.location.href = base_url + "refacciones/factura/listado/";
                });
            }
            if (header.status == 500) {
                let titulo = "Error subiendo factura!"
                utils.displayWarningDialog(titulo, 'warning', function (result) {});
            }
        })

    })

});