var _appsFunction = function () {
	this.init = function () {},
	this.get = function () {
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,

			ajax: {
				url: PATH + '/nomina/inicio/consultanomina/obtener_listado',
				type: "POST",
				data: function(){
					return {periodo:periodo};
				}
			},
			columns: [
				{
					title: 'Clave',
					data: 'Clave'
				},
				{
					title: 'Nombre Completo',
					'data': function(data){
						var row = [
							data.Nombre,
							data.Apellido_1,
							data.Apellido_2
						];
						return row.join(' ');
					}
				},
				{
					title: 'Total de percepciones',
					'data': 'montos',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data.percepcion);
					}
				},
				{
					title: 'Total de deducciones',
					'data': 'montos',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data.deduccion);
					}
				},
				{
					title: 'Neto a pagar',
					'data': 'montos',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data.monto);
					}
				}
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
