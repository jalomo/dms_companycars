var _appsFunction = function () {
	this.init = function () {},
	this.get = function () {
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			ajax: {
				url: PATH + '/nomina/inicio/trabajador/historico_trabajador_get?identity='+identity,
				type: "GET"
			},
			columns: [
				{
					title: 'Tipo de movimiento',
					data: 'Descripcion_HistTipoMov'
				},
				{
					title: 'Fecha del movimiento',
					data: 'FechaMovimiento'
				},
				{
					title: 'Salario diario',
					data: 'SalarioDiario'
				},
				{
					title: 'SDI IMSS',
					data: 'SdiImss'
				},
				{
					title: 'Parte fija',
					data: 'ParteFija'
				},
				{
					title: 'Parte variable',
					data: 'ParteVariable'
				}
				
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
