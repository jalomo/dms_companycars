var _appsFunction = function () {
	this.init = function () {},
		// this.delete = function ($this) {
		// 	Swal.fire({
		// 		icon: 'warning',
		// 		title: '¿Esta seguro de eliminar el registro?',
		// 		//text: response.info,
		// 		showCancelButton: true,
		// 		confirmButtonText: "Si",
		// 		cancelButtonText: "No"
		// 	}).then((result) => {
		// 		if (result.value) {

		// 			$.ajax({
		// 				dataType: "json",
		// 				type: 'delete',
		// 				url: NOMINA_API + 'catalogos/clasificaciones/store',
		// 				data: {
		// 					'identity': $($this).attr('data-id')
		// 				},
		// 				success: function (response, status, xhr) {
		// 					Swal.fire({
		// 						icon: 'success',
		// 						title: '',
		// 						text: response.info,
		// 						confirmButtonText: "Aceptar"
		// 					}).then((result) => {
		// 						var table = $('table#listado').DataTable();
		// 						table.ajax.reload(null, false);
		// 					});
		// 				}

		// 			});
		// 		}
		// 	});
		// },
	this.get = function () {
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,

			ajax: {
				url: PATH + '/nomina/inicio/percepcionesdeducciones/index_get',
				type: "POST"
			},
			columns: [
				{
					title: 'P/D',
					data: 'Clave'
				},
				{
					title: 'Descripción',
					data: 'Descripcion'
				},
				{
					title: 'Fórmula',
					data: 'Formula'
				},
				{
					title: '-',
					'data': function (data) {
						return "<a class='btn btn-success btn-sm' href='" + PATH + '/nomina/inicio/percepcionesdeducciones/cambios?id=' + btoa(data.id) + "' title='Datos generales' > <i class='far fa-list-alt' aria-hidden='true'></i> </a>";
					}
				},
				{
					title: '-',
					'data': function (data) {
						if(data.cve_pd == 'P'){
							return "<a class='btn btn-info btn-sm' href='" + PATH + '/nomina/inicio/percepcionesdeducciones/basefiscales/'+data.id +"' title='Bases fiscales' > <i class='fas fa-money-check'></i> </a>";
						}
						return '';
					}
				}
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
