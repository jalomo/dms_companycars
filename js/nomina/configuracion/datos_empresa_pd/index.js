var _appsFunction = function () {
	this.init = function () {

		// var change_input = {
		// 	"Logo": {type: "hidden"},
		// 	"LogoArchivo": {type: "hidden"},
		// 	"NumeroEmpresa": {type: "number",max:"10",min:"1"},
		// 	"NoDeSerie": {type: "number",max:"2",min:"1"},
		// 	"CURP": {type: "text",max:"18",onblur:"Apps.validarCurp(this)"},
		// 	"RFC": {type: "text",onblur:"Apps.validarRfc(this)"}
		// };
		// update_inputs(change_input);
		update_select2('form#general_form select');
		
		// if($('input[name=LogoArchivo]').val().length > 0 && $('input[name=Logo]').val().length > 0){
		// 	$('label[for=imagen_logo]').html( $('input[name=Logo]').val() );
		// }
		
	},

	
	this.guardar = function () {
		Apps.guardarFormulario();
	}	

	this.guardarFormulario = function(){

		 var data_send =  $('form#general_form').serializeArray();
		 data_send.push({ 'name': 'id','value':id });

        $.ajax({
            dataType: "json",
			cache: false,
			type: 'POST',
            url: PATH + '/nomina/configuracion/datos_empresa_pd/editar_guardar',
            data: data_send,
            success: function (response, status, xhr) {

				if (response.status == 'success') {
					Swal.fire({
						icon: 'success',
						title: '',
						text: response.message,
						confirmButtonText: "Aceptar"
					});
				}

            }

        });
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.init();
});
