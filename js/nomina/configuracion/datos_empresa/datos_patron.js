var _appsFunction = function () {
    this.init = function () {},

    
    this.addContenido = function(){
		window.location.href = PATH + '/nomina/configuracion/datos_empresa/datos_patron_alta/'+id;
    },

    this.editContenido = function(id_Registro){
		window.location.href = PATH + '/nomina/configuracion/datos_empresa/datos_patron_editar/'+id+'/'+id_Registro;
    },

    this.delete = function ($this) {
		Swal.fire({
			icon: 'warning',
			title: '¿Esta seguro de eliminar el registro?',
			//text: response.info,
			showCancelButton: true,
			confirmButtonText: "Si",
			cancelButtonText: "No"
		}).then((result) => {
			if (result.value) {

				$.ajax({
					dataType: "json",
					type: 'post',
					url: PATH + '/nomina/configuracion/datos_empresa/datos_patron_delete',
					data: {
						'id': $($this).attr('data-id')
					},
					success: function (response, status, xhr) {
						Swal.fire({
							icon: 'success',
							title: '',
							text: response.message,
							confirmButtonText: "Aceptar"
						}).then((result) => {
							var table = $('table#listado').DataTable();
							table.ajax.reload(null, false);
						});
					}

				});
			}
		});
	},
    
   
	this.get = function () {
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			ajax: {
				url: PATH + '/nomina/configuracion/datos_empresa/datos_patron_get?id='+id,
				type: "POST"
			},
			columns: [
				{
					title: 'Clave',
					data: 'Clave'
                },
                {
					title: 'Registro',
					data: 'RegistroPatronal'
                },
                // {
				// 	title: 'Riesgo',
				// 	data: 'id_RiesgoTrabajo'
                // },
                {
					title: 'Salario mínimo',
					data: 'SalarioMinimo'
                },
                {
					title: 'Área geografica',
					data: 'AreaGeografica'
                },
                {
					title: 'Lugar Exp.',
					data: 'LugarExpedicion'
                },
                {
					title: 'CP',
					data: 'CP'
                },
				{
					title: '-',
					'data': function (data) {
						return "<button  value='"+data.id+"'  type='button' class='btn btn-info btn-sm' href='#' onclick='Apps.editContenido(this.value)' title='Editar' > <i class='fas fa-pencil-alt'></i> </button>";
					}
                },
                {
					title: '-',
					'data': function (data) {
						return "<button type='button' onclick='Apps.delete(this);' class='btn-borrar btn btn-danger btn-sm' data-id=" + data.id + "><i class='fas fa-trash'></i></button>";
					}
				}
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
