var _appsFunction = function () {
    this.nomina =  [
        {
            'text': 'Trabajador',
            'state': { 'opened': true},
            'children': [
                {'text': 'Trabajadores','icon': 'jstree-file',"a_attr": {"href": PATH + "/nomina/inicio/trabajador/"}},
                {'text': 'Nómina del trabajador','icon': 'jstree-file',"a_attr": {"href": '#'}},
                {'text': 'Consulta de nómina','icon': 'jstree-file',"a_attr": {"href": '#'}},
                {'text': 'Reporte de la nómina','icon': 'jstree-file',"a_attr": {"href": '#'}}
            ]
        },
        {'text': 'Tablero de nómina','state': {'opened': false},'children': false},
        {'text': 'Percepciones y deducciones','state': {'opened': false},'children': false},
        {'text': 'Movimiento','state': {'opened': false},'children': false},
        {'text': 'eRecibos','state': {'opened': false},'children': false},
        {'text': 'Dispersión','state': {'opened': false},'children': false},
        {'text': 'Correo','state': {'opened': false},'children': false},
        {'text': 'Depositos de doctos','state': {'opened': false},'children': false},
        {'text': 'Cierre de nómina','state': {'opened': false},'children': false},
        {'text': 'Párametos de la nómina','state': {'opened': false},'children': false}
    ],
    this.catalogosconsultas =  [
        {
            'text': 'Catálogos del Sistema',
            'state': { 'opened': true},
            'children': [
                {'text': 'Tablas del sistema','icon': 'jstree-file',"a_attr": {"href": PATH + "/nomina/catalogosconsultas/tablassistema"}}
            ]
        },
        {
            'text': 'Organización',
            'stakte': {'opened': false},
            'children': [
                {'text': 'Departamentos',"icon": 'jstree-file',"a_attr": {"href": PATH + "/nomina/catalogosconsultas/departamentos"}},
                {'text': 'Puestos',"icon": 'jstree-file',"a_attr": {"href": PATH + "/nomina/catalogosconsultas/puestos"}},
                {'text': 'Clasificaciones',"icon": 'jstree-file',"a_attr": {"href": PATH + "/nomina/catalogosconsultas/clasificaciones"}}
            ]
        },
        {'text': 'Percepciones y deducciones','state': {'opened': false},'children': false},
        {'text': 'Consulta de movientos','state': {'opened': false},'children': false}
    ],
    this.acumulados =  [
        {'text': 'Bases fiscales','state': {'opened': false},'children': false},
        {'text': 'Percepciones y deducciones','state': {'opened': false},'children': false},
        {'text': 'Acumulados del trabajador','state': {'opened': false},'children': false},
        {'text': 'Acumulados de nóminas','state': {'opened': false},'children': false},
        {'text': 'Recibos electrónicos','state': {'opened': false},'children': false}
    ],

    this.render = function (menu) {
        var dom = $('div#jstree_menu_div');
        dom.jstree({
            'core': {
                'themes': {
                    'variant': 'large',
                    'stripes': true
                },
                'data': menu
            },

            "plugins": ["dnd", "contextmenu"]
        });
        dom.on("changed.jstree", function (e, data) {
            try {
                if (data.node.a_attr.href != '#') {
                    window.location.href = data.node.a_attr.href;
                }
            } catch (error) {
                window.location.reload();
            }
        });
        // dom.on('ready.jstree', function() {
        //     dom.jstree("open_all");          
        // });
    }

};

var Apps;
$(function () {
    Apps = new _appsFunction();
    switch (identity) {
        case 'catalogosconsultas':
            $('#tituloMenu').html('Catalogos y consultas');
            Apps.render(Apps.catalogosconsultas);
            break;
        case 'acumulados':
            $('#tituloMenu').html('Acumulados');
            Apps.render(Apps.acumulados);
            break;
    
        default:
            $('#tituloMenu').html('Nomina');
            Apps.render(Apps.nomina);
            break;
    }
});
