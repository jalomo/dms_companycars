var _appsFunction = function () {
	this.init = function () {},
		this.delete = function ($this) {
			Swal.fire({
				icon: 'warning',
				title: '¿Esta seguro de eliminar el registro?',
				//text: response.info,
				showCancelButton: true,
				confirmButtonText: "Si",
				cancelButtonText: "No"
			}).then((result) => {
				if (result.value) {

					$.ajax({
						dataType: "json",
						type: 'post',
						url: PATH + '/nomina/api/api/runner/clasificaciones/delete',
						data: {
							'id': $($this).attr('data-id')
						},
						success: function (response, status, xhr) {
							Swal.fire({
								icon: 'success',
								title: '',
								text: response.message,
								confirmButtonText: "Aceptar"
							}).then((result) => {
								var table = $('table#listado').DataTable();
								table.ajax.reload(null, false);
							});
						}

					});
				}
			});
		},
		this.get = function () {
			$('table#listado').dataTable({
				"ajax": {
					"url": PATH + '/nomina/api/api/runner/clasificaciones/get',
					"type": "GET"
				},
				columns: [{
						'data': 'Clave'
					},
					{
						'data': 'Descripcion'
					},
					{
						'data': function (data) {
							return "<a class='btn btn-success' href='" + PATH + '/nomina/catalogosconsultas/clasificaciones/editar?id=' + data.id + "'> Editar </a>";
						}
					},
					{
						'data': function (data) {
							return "<button type='button' onclick='Apps.delete(this);' class='btn-borrar btn btn-danger' data-id=" + data.id + ">Borrar</button>";
						}
					}
				]
			})


		}

}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
