<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Seminuevos</div>
                <a class="nav-link" href="<?php echo base_url('menu/'); ?>">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    DMS 
                </a>

                <div class="sb-sidenav-menu-heading">Menu</div>
               
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#colapse_resepcion" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Recepción
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="colapse_resepcion" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('seminuevos/Unidades/index'); ?>">Unidades</a>
                        <a class="nav-link" href="<?php echo base_url('seminuevos/Principal/construccion'); ?>">Control de Doctos</a>
                    </nav>
                </div>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#colapse_os" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Orden de servicio
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="colapse_os" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('seminuevos/Consultas/index'); ?>">Consultas</a>
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#colapse_entradas" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Ventas
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="colapse_entradas" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('seminuevos/Principal/construccion'); ?>">Pre-pedidos</a>
                        <a class="nav-link" href="<?php echo base_url('seminuevos/Principal/construccion'); ?>">Facturas</a>
                        <a class="nav-link" href="<?php echo base_url('seminuevos/Principal/construccion'); ?>">Salidas</a>
                        <a class="nav-link" href="<?php echo base_url('seminuevos/Principal/construccion'); ?>">Casta Factura</a>
                        <a class="nav-link" href="<?php echo base_url('seminuevos/Principal/construccion'); ?>">Consultas</a>
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#colapse_reportes" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Reportes
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="colapse_reportes" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('seminuevos/Reportes/reporteinventario'); ?>">Inventario</a>
                        <a class="nav-link" href="<?php echo base_url('seminuevos/Reportes/reportependientes'); ?>">Pendientes por facturar</a>
                        <a class="nav-link" href="<?php echo base_url('seminuevos/Reportes/reporteventadiarias'); ?>">Ventas diarias</a>
                        <a class="nav-link" href="<?php echo base_url('seminuevos/Reportes/reporteventavendedor'); ?>">Ventas por vendedor</a>
                        <a class="nav-link" href="<?php echo base_url('seminuevos/Reportes/reportepolizacompra'); ?>">Póliza de ventas</a>
                        <a class="nav-link" href="<?php echo base_url('seminuevos/Reportes/reportepolizacompras'); ?>">Póliza de compras</a>
                        <a class="nav-link" href="<?php echo base_url('seminuevos/Reportes/reportecomisiones'); ?>">Comisiones</a>
                    </nav>
                </div>
                
                <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                        <a class="nav-link collapsed" href="<?php echo base_url('refacciones/productos/listado'); ?>" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                            Authentication
                            <div class="sb-sidenav-collapse-arrow">
                                <i class="fas fa-angle-down"></i>
                            </div>
                        </a>
                        <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="login.html">Login</a>
                                <a class="nav-link" href="register.html">Register</a>
                                <a class="nav-link" href="password.html">Forgot Password</a>
                            </nav>
                        </div>
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">Error
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div></a>
                        <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav"><a class="nav-link" href="401.html">401 Page</a><a class="nav-link" href="404.html">404 Page</a><a class="nav-link" href="500.html">500 Page</a></nav>
                        </div>
                    </nav>
                </div>
                
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Usuario:</div>
            En desarrollo
        </div>
    </nav>
</div>