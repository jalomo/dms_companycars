<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Notificaciones extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
    $this->load->library('curl');

  }

  /*
  * funcion que inicia el controlador.
  * desarrollo: jalomo
  * esta funcion estara para  listar los catalogos
  */
  public function mis_contactos()
  {
    $data['titulo'] = "";
    $api = $this->curl->curlGet('api/usuarios');
    $data['usuarios'] = procesarResponseApiJsonToArray($api);
    $this->blade->render('notificaciones/contactos',$data);
  }

  public function mis_notificaciones()
  {
    $usuario_id = $this->session->userdata('id');
    $api = $this->curl->curlGet('api/usuarios/' . $usuario_id);
    $data['usuario'] = procesarResponseApiJsonToArray($api);
    $this->blade->render('notificaciones/mis_notificaciones',$data);
  }
  public function chat()
  {
    $data['destinatario'] = $this->input->get('destinatario');

    $this->blade->render('notificaciones/chat',$data);
  }
}
