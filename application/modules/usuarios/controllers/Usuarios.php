<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Usuarios extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
    $this->load->library('curl');
    $this->load->helper('general');
  }

  public function perfil()
  {
    $data['titulo'] = ""; //"Contabilidad/Consultas/polizas";
    $this->blade->render('usuarios/perfil', $data);
  }

  public function registro()
  {
    $data['titulo'] = ""; //"Contabilidad/Consultas/polizas";
    $this->blade->render('usuarios/registro', $data);
  }

  public function roles()
  {
    $data['modulo'] = "Administrar";
    $data['subtitulo'] = "Roles";
    $data['submodulo'] = "Admin";

    $roles = $this->curl->curlGet('api/roles');
    $dataRoles = procesarResponseApiJsonToArray($roles);
    $data['roles'] = isset($dataRoles) ? $dataRoles : [];

    $this->blade->render('usuarios/administrador_roles', $data);
  }

  public function index()
  {
    $data['modulo'] = "Administrar modulos y Menú";
    $data['subtitulo'] = "menú y roles";
    $data['submodulo'] = "Modulos por usuarios";
    $usuarios = $this->curl->curlGet('api/usuarios');
    $dataUsuarios = procesarResponseApiJsonToArray($usuarios);
    $data['usuarios'] = isset($dataUsuarios) ? $dataUsuarios : [];

    $modulos = $this->curl->curlGet('api/menu/modulos');
    $dataregistros = procesarResponseApiJsonToArray($modulos);
    $data['modulos'] = isset($dataregistros) ? $dataregistros : [];

    $this->blade->render('usuarios/usuarios_modulos', $data);
  }

  public function verpermisos()
  {
    $data['modulo'] = "Administrar modulos y Menú";
    $data['subtitulo'] = "menú y roles";
    $data['submodulo'] = "Modulos por usuarios";
    $usuarios = $this->curl->curlGet('api/usuarios');
    $dataUsuarios = procesarResponseApiJsonToArray($usuarios);
    $data['usuarios'] = isset($dataUsuarios) ? $dataUsuarios : [];
    
    $modulos = $this->curl->curlGet('api/menu/modulos');
    $dataregistros = procesarResponseApiJsonToArray($modulos);
    $data['modulos'] = isset($dataregistros) ? $dataregistros : [];
    
    $this->blade->render('usuarios/verpemisos', $data);
  }

  public function administrarUsuarios()
  {
    $data['modulo'] = "Administrar";
    $data['subtitulo'] = "Usuarios";
    $data['submodulo'] = "Modulos usuarios";
    $usuarios = $this->curl->curlGet('api/usuarios');
    $dataUsuarios = procesarResponseApiJsonToArray($usuarios);
    $data['usuarios'] = isset($dataUsuarios) ? $dataUsuarios : [];
    $this->blade->render('usuarios/admin_usuarios', $data);
  }

  public function editarUsuario($id = '')
  {
    $data['modulo'] = "Administrar";
    $data['subtitulo'] = "Usuarios";
    $data['submodulo'] = "Modulos usuarios";
    $roles = $this->curl->curlGet('api/roles');
    $dataRoles = procesarResponseApiJsonToArray($roles);
    $data['roles'] = isset($dataRoles) ? $dataRoles : [];
    $usuarios = $this->curl->curlGet('api/usuarios/' . $id);
    $dataUsuarios = procesarResponseApiJsonToArray($usuarios);
    $data['usuarios'] = isset($dataUsuarios) ? $dataUsuarios : [];
    $this->blade->render('usuarios/editar_usuario', $data);
  }

  public function modulos()
  {
    $modulos = $this->curl->curlGet('api/menu/modulos');
    $dataModulos = procesarResponseApiJsonToArray($modulos);
    $data['modulos'] = isset($dataModulos) ? $dataModulos : [];
    $this->blade->render('usuarios/modulos_menu/administrador_modulos', $data);
  }

  public function permisosVista()
  {
    $data['modulo'] = "Administrar";
    $data['subtitulo'] = "menú y roles";
    $data['submodulo'] = "Pantallas por roles";
    $usuarios = $this->curl->curlGet('api/usuarios');
    $dataUsuarios = procesarResponseApiJsonToArray($usuarios);
    $data['usuarios'] = isset($dataUsuarios) ? $dataUsuarios : [];
    // $roles = $this->curl->curlGet('api/roles');
    // $dataRoles = procesarResponseApiJsonToArray($roles);
    // $data['roles'] = isset($dataRoles) ? $dataRoles : [];
    $modulos = $this->curl->curlGet('api/menu/modulos');
    $dataregistros = procesarResponseApiJsonToArray($modulos);
    $data['modulos'] = isset($dataregistros) ? $dataregistros : [];
    $this->blade->render('usuarios/modulos_menu/roles_modulos_vistas', $data);
  }
}
