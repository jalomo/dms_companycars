@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <h1>Permisos por pantallas</h1>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Usuario:</label>
                <select class="form-control" id="usuario_id" name="usuario_id" style="width: 100%;">
                    <option value="">Selecionar ...</option>
                    @if(!empty($usuarios))
                    @foreach ($usuarios as $usuario)
                        <option value="{{ $usuario->id}}"> {{$usuario->usuario}} - {{ $usuario->nombre. " ".$usuario->apellido_paterno }}</option>
                        @endforeach
                    @endif
                </select>
                <div id="usuario_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="seccion">Modulos</label>
                <select class="form-control dinamic-seccion-vista" name="seccion_modulo_vista_id" id="seccion_modulo_vista_id">
                </select>
                <div id="seccion_modulo_vista_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="seccion">Secciones</label>
                <select class="form-control dinamic-seccion-vista" name="seccion_vista_id" id="seccion_vista_id">
                </select>
                <div id="seccion_vista_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="seccion">Submenus</label>
                <select class="form-control dinamic-submenu-vista" name="submenu_vista_id" id="submenu_vista_id">
                </select>
                <div id="submenu_vista_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="d-none" id="check_all">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="check_todos">
                    <label class="form-check-label">Check todos</label>
                </div>
            </div>
            <br>
            <div class="" id="checks_vistas_container"></div>
        </div>
        <div class="col-md-12">
            <button id="btn-guardar-vista" class="btn btn-primary" type="button">
                Guardar vista
            </button>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#usuario_id").on('change', function() {
        let usuario_id = $("#usuario_id").val();
        ajax.get('api/usuarios/modulos-by-usuario?usuario_id=' + usuario_id, {}, function(response, headers) {
            $('#seccion_modulo_vista_id').html('');
            $('#seccion_modulo_vista_id').append('<option value="">Seleccionar ...</option>');
            if(response.length > 0){
                response.map(item => {
                    $('#seccion_modulo_vista_id').append('<option value="' + item.modulo_id + '">' + item.nombre_modulo + '</option>');
                })
            }else{
                toastr.warning("No modulos asignados...."); 
            }
        })
    });

    //inicio vistas
    $("#seccion_modulo_vista_id").on('change', function() {
        let modulo_id = $("#seccion_modulo_vista_id").val();
        ajax.get('api/menu-secciones?modulo_id=' + modulo_id, {}, function(response, headers) {
            $('#seccion_vista_id').html('');
            $('#seccion_vista_id').append('<option value="">Seleccionar ...</option>');
            response.map(item => {
                $('#seccion_vista_id').append('<option value="' + item.id + '">' + item.nombre + '</option>');
            })
        });
    });

    $("#check_todos").on('change', function() {
        $("input[type=checkbox]").each(function() {
            let vista_id = $(this).data('item_id');
            if(vista_id){
                document.getElementById("vista_" + vista_id).checked = document.getElementById("check_todos").checked;
            }
        });
    });


    $("#seccion_vista_id").on('change', function() {
        let seccion_id = $("#seccion_vista_id").val();
        ajax.get('api/menu-submenu?seccion_id=' + seccion_id, {}, function(response, headers) {
            $('#submenu_vista_id').html('');
            $('#checks_vistas_container').html('');
            document.getElementById("check_all").classList.add("d-none");
            $('#submenu_vista_id').append('<option value=""> Seleccionar ...</option>');
            response.map(item => {
                $('#submenu_vista_id').append('<option value="' + item.id + '">' + item.nombre_submenu + '</option>');
            })
        });
    });

    $("#submenu_vista_id").on('change', function() {
        let submenu_id = $("#submenu_vista_id").val();
        ajax.get('api/menu-vistas?submenu_id=' + submenu_id, {}, function(response, headers) {

            if (response.length > 0) {
                $('#checks_vistas_container').html('');
                document.getElementById("check_all").classList.remove("d-none");
                document.getElementById("check_todos").checked = false;
                response.map(item => {
                    $('#checks_vistas_container').append(
                        '<div class="form-check">' +
                        '<input class="form-check-input" type="checkbox" id="vista_' + item.vista_id + '" data-item_id="' + item.vista_id + '">' +
                        '<label class="form-check-label">' + item.nombre_vista + '</label>' +
                        '</div>'
                    );
                })

                ajax.get('api/menu-vistas/get-by-usuario?usuario_id=' + $("#usuario_id").val(), {}, function(response_selected, headers) {
                    let array_items = [];
                    array_items = [...array_items,
                        response_selected.map(item => item.vista_id)
                    ]

                    $("input[type=checkbox]").each(function() {
                        let vista_id = $(this).data('item_id');
                        if (array_items[0].includes(vista_id)) {
                            document.getElementById("vista_" + vista_id).checked = true;
                        }
                    });
                });
            } else {

                document.getElementById("check_all").classList.add("d-none");
                toastr.warning("No hay pantallas asignadas....");
                $('#checks_vistas_container').html('');
            }
        });
    });

    $("#btn-guardar-vista").on('click', function() {
        if ($("#seccion_modulo_vista_id").val() == '') {
            toastr.error("Seleccionar modulo de menu...");
            return false;
        }

        if ($("#usuario_id").val() == '') {
            toastr.error("Seleccionar usuario...");
            return false;
        }


        if ($("#seccion_vista_id").val() == '') {
            toastr.error("Seleccionar seccion de menu...");
            return false;
        }

        if ($("#submenu_vista_id").val() == '') {
            toastr.error("Seleccionar submenu ...");
            return false;
        }

        let vista_array = [];
        $("input[type=checkbox]:checked").each(function() {
            let vista_id = $(this).data('item_id');
            if(vista_id){
                vista_array.push(vista_id);
            }
        });

        if (vista_array.length  == 0) {
            toastr.error("Seleccionar vistas....");
            return false;
        }

        //guardar
        ajax.post('api/menu-vistas/store-by-usuarios', {
            "usuario_id": $("#usuario_id").val(),
            "seccion_id": $("#seccion_vista_id").val(),
            "submenu_id": $("#submenu_vista_id").val(),
            "vista_id": vista_array
        }, function(response, headers) {
            if (headers.status == 201) {
                toastr.success("Permisos creados....");
            }
        })
    });

    //fin vistas
</script>
@endsection