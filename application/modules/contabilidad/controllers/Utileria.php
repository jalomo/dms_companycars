<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Utileria extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
  }

  public function cierre_mes()
  {
  	$data['titulo'] = "Contabilidad/Utileria/Cierre de mes";
    $this->blade->render('utileria/cierre_mes',$data);
  }

  public function ejercicios()
  {
    $data['titulo'] = "Contabilidad/Reportes/Todos los ejercicios";
    $this->blade->render('utileria/ejercicios',$data);
  }
}
