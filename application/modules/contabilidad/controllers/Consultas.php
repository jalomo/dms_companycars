<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Consultas extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
  }

  public function polizas()
  {
  	$data['titulo'] = "Contabilidad/Consultas/polizas";
    $this->blade->render('consultas/polizas',$data);
  }

  public function saldos()
  {
  	$data['titulo'] = "Contabilidad/Catalogos/saldos";
    $this->blade->render('consultas/saldos',$data);
  }

 
}