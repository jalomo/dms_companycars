<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Salidas extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time', 300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $dataFromApi = $this->curl->curlGet('api/salida-unidades');
        $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = isset($dataregistros) ? $dataregistros : [];

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Inventario";
        $data['titulo'] = "Salida de Unidades";
        $data['subtitulo'] = "Listado";

        $this->blade->render('salidas/listado', $data);
    }

    public function alta($id = '')
    {
        //Si se recupera un id en el alta, cargamos la informacion de la unidad

        if (!$id) {
            return $this->index();
        }

        //Recuperamos la informacion del registro
        $dataFromApi = $this->curl->curlGet('api/recepcion-unidades/' . $id);
        $valores = json_decode($dataFromApi, TRUE);
        
        $modelos = $this->curl->curlGet('api/catalogo-anio');
        $data['cat_modelos'] = procesarResponseApiJsonToArray($modelos);

        $anio = $this->curl->curlGet('api/catalogo-anio');
        $data['cat_anio'] = procesarResponseApiJsonToArray($anio);

        $color = $this->curl->curlGet('api/catalogo-colores');
        $data['cat_color'] = procesarResponseApiJsonToArray($color);
        
        $autos = $this->curl->curlGet('api/catalogo-autos');
        $data['catalogo_autos']= procesarResponseApiJsonToArray($autos);
        if (count($valores) > 0) {
            $data_unidad = $valores[0];
            // utils::pre($data_unidad);
            $data["unidad"] = $data_unidad;

            // if ($data_unidad['color_id'] != "0") {
            //     foreach ($cat_color as $color) {
            //         if ($data_unidad['color_id'] == $color->id) {
            //             $data["color"] = $color->nombre;
            //         }
            //     }
            // } else {
            //     $data["color"] = "OTRO";
            // }

            // if ($data_unidad['anio_id'] != "0") {
            //     foreach ($cat_anio as $anio) {
            //         if ($data_unidad['anio_id'] == $anio->id) {
            //             $data["modelo"] = $anio->nombre;
            //         }
            //     }
            // } else {
            //     $data["modelo"] = "OTRO";
            // }

            // if ($data_unidad['catalogo_id'] != "0") {
            //     foreach ($cat_unidades as $unidad) {
            //         if ($data_unidad['catalogo_id'] == $unidad->id) {
            //             $data["catalogo"] = $unidad->clave;
            //         }
            //     }
            // } else {
            //     $data["catalogo"] = "OTRO";
            // }
        }

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Inventario";
        $data['titulo'] = "Salida de Unidades";
        $data['subtitulo'] = "Registro";

        $this->blade->render('salidas/alta', $data);
    }

    public function relacion_salida($id = '')
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');

            //Recuperamos la informacion del registro
            $dataFromApi = $this->curl->curlGet('api/recepcion-unidades/' . $id);
            $dataregistro = procesarResponseApiJsonToArray($dataFromApi);
            $data = isset($dataregistro)  ? $dataregistro[0] : [];

            //Verificamos si se cargaron los datos
            if (isset($data->catalogo_id)) {
                $autos = $this->curl->curlGet('api/unidades');
                $cat_unidades = procesarResponseApiJsonToArray($autos);

                foreach ($cat_unidades as $unidades) {
                    if ($unidades->id == $data->catalogo_id) {
                        $data["catalogo"] = $unidades->clave;
                    }
                }

                $color = $this->curl->curlGet('api/catalogo-colores');
                $cat_color = procesarResponseApiJsonToArray($color);

                foreach ($cat_color as $color) {
                    if ($color->id == $data->color_id) {
                        $data["color"] = $color->nombre;
                    }
                }

                $data["no_economico"] = $data->n_economico;
                $data["no_serie"] = $data->vin;
                $data["vehiculo"] = $data->unidad_descripcion;
            }

            $data['modulo'] = "Autos";
            $data['submodulo'] = "Inventario";
            $data['titulo'] = "Salida de Unidades";
            $data['subtitulo'] = "Recepción-Salida";

            $this->blade->render('salidas/alta', $data);
        }
    }

    public function editar($id = '')
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');

            $dataFromApi = $this->curl->curlGet('api/salida-unidades/' . $id);
            $dataregistro = procesarResponseApiJsonToArray($dataFromApi);
            $data['data'] = isset($dataregistro)  ? $dataregistro : [];

            $data['modulo'] = "Autos";
            $data['submodulo'] = "Inventario";
            $data['titulo'] = "Salida de Unidades";
            $data['subtitulo'] = "Edición";

            $this->blade->render('salidas/alta', $data);
        }
    }

    public function vista_pdf($id = '')
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');

            $dataFromApi = $this->curl->curlGet('api/salida-unidades/' . $id);
            $dataregistro = procesarResponseApiJsonToArray($dataFromApi);
            $data['data'] = isset($dataregistro)  ? $dataregistro : [];

            $data['modulo'] = "Autos";
            $data['submodulo'] = "Inventario";
            $data['titulo'] = "Salida de Unidades";
            $data['subtitulo'] = "Edición";

            $view = $this->load->view('salidas/plantillapdf', $data, TRUE);
            $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
        }
    }

    //Convertir canvas base64 a imagen (firmas)
    public function firmas()
    {
        $imgBase64 = $_POST['base_64'];
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi") . "_";
        for ($i = 0; $i <= 3; $i++) {
            $urlNombre .= substr($str, rand(0, strlen($str) - 1), 1);
        }

        $urlDoc = 'img/firmas';

        //Validamos que exista la carpeta destino   base_url()
        if (!file_exists($urlDoc)) {
            mkdir($urlDoc, 0647, true);
        }

        $data = explode(',', $imgBase64);
        //Comprobamos que se corte bien la cadena
        if ($data[0] == "data:image/png;base64") {
            //Creamos la ruta donde se guardara la firma y su extensión
            if (strlen($imgBase64) > 1) {
                $base = 'img/firmas/' . $urlNombre . ".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            } else {
                $base = "";
            }
        } else {
            if (substr($imgBase64, 0, 4) == "img/") {
                $base = $imgBase64;
            } else {
                $base = "";
            }
        }

        echo $base;
    }
}
