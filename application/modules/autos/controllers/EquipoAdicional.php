<?php

defined('BASEPATH') or exit('No direct script access allowed');

class EquipoAdicional extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time', 300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function index()
    {
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Equipo adicional";
        $data['titulo'] = "Agregar equipo";
        $data['subtitulo'] = "Lista unidades en pre venta";

        $this->blade->render('equipoAdicional/index_equipo_adicional', $data);
    }

    public function unidadesParaEquipoOpcional()
    {
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Equipo adicional";
        $data['titulo'] = "Unidades";
        $data['subtitulo'] = "Lista con equipo adicional";

        $this->blade->render('equipoAdicional/unidades_equipo', $data);
    }

    public function agregarEquipo($id_preventa_unidad = '')
    {
        $data['modulo'] = "Equipo adicional";
        $data['submodulo'] = "Inventario";
        $data['titulo'] = "Agregar equipo";
        $data['subtitulo'] = "Lista unidades";

        $dataFromApi = $this->curl->curlGet('api/autos/pre-pedidos/' . $id_preventa_unidad);
        $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
        $data['data_preventa'] = isset($dataregistros) ? $dataregistros[0] : [];
        
        $catalogos = $this->returnCatalogosUnidad();
        $data['cat_cfdi'] = $catalogos['cat_cfdi'];
        $data['nombre_usuario'] =  $catalogos['nombre_usuario'];
        $data['id_vendedor'] = $catalogos['id_vendedor'];
        $data['catalogo_unidades'] = $catalogos['catalogo_unidades'];
        $data['catalogo_modelos'] = $catalogos['catalogo_modelos'];
        $data['catalogo_marcas'] = $catalogos['catalogo_marcas'];
        $data['catalogo_anio'] = $catalogos['catalogo_anio'];
        $data['catalogo_color'] = $catalogos['catalogo_color'];
        $data['catalogo_clientes'] = $catalogos['catalogo_clientes'];
        $data['plazo_credito'] = $catalogos['plazo_credito'];
        $data['tipo_forma_pago'] = $catalogos['tipo_forma_pago'];
        $data['tipo_pago'] = $catalogos['tipo_pago'];
        $cat_productos = $this->curl->curlGet('api/productos/stockActual');
        $data['listado_productos'] = procesarResponseApiJsonToArray($cat_productos);

        $this->blade->render('equipoAdicional/agregar_equipo', $data);
    }

    public function returnCatalogosUnidad()
    {
        $cfdi = $this->curl->curlGet('api/cfdi');
        $unidades = $this->curl->curlGet('api/unidades');
        $modelos = $this->curl->curlGet('api/catalogo-autos');
        $marcas = $this->curl->curlGet('api/catalogo-marcas');
        $anio = $this->curl->curlGet('api/catalogo-anio');
        $color = $this->curl->curlGet('api/catalogo-colores');
        $clientes = $this->curl->curlGet('api/clientes/tipo-clave?clave=CV');
        $plazo_credito = $this->curl->curlGet('api/catalogo-plazo-credito');
        $tipo_forma_pago = $this->curl->curlGet('api/catalogo-tipo-forma-pago');
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');

        $data['nombre_usuario'] = $this->session->userdata('nombre') . " " . $this->session->userdata('apellido_paterno') . " " . $this->session->userdata('apellido_materno');
        $data['id_vendedor'] = $this->session->userdata('id');

        $data['cat_cfdi'] = procesarResponseApiJsonToArray($cfdi);
        $data['catalogo_unidades'] = procesarResponseApiJsonToArray($unidades);
        $data['catalogo_modelos'] = procesarResponseApiJsonToArray($modelos);
        $data['catalogo_marcas'] = procesarResponseApiJsonToArray($marcas);
        $data['catalogo_anio'] = procesarResponseApiJsonToArray($anio);
        $data['catalogo_color'] = procesarResponseApiJsonToArray($color);
        $data['catalogo_clientes'] = procesarResponseApiJsonToArray($clientes);
        $data['plazo_credito'] = procesarResponseApiJsonToArray($plazo_credito);;
        $data['tipo_forma_pago'] = procesarResponseApiJsonToArray($tipo_forma_pago);
        $data['tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);

        return $data;
    }

    public function ajax_detalle_equipo_carrito()
    {
        if ($this->input->post('preventa_id') == '') {
            $data['data'] = [];
            echo json_encode($data);
            die();
        }

        $detalleVenta = $this->curl->curlGet('api/autos/equipo-opcional/' . $this->input->post('preventa_id'));
        $detalle = procesarResponseApiJsonToArray($detalleVenta);
        $data['data'] = isset($detalle) ? $detalle : [];
        echo json_encode($data);
    }
}
