<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Reportes extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time', 300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function corteCaja()
    {
        $data['titulo'] = "Caja";
        $data['subtitulo'] = "Corte de caja";
        $this->blade->render('reportes/listado_corte', $data);
    }

    public function nuevo_corte()
    {

        $fecha_actual = date('Y-m-d');
        $usuario_registro = $this->session->userdata('id');
        // $fecha_actual = '2020-11-19';
        $api_valida = $this->curl->curlGet('api/corte-caja/mostrarPorUsuarioAndFecha?usuario_registro=' . $usuario_registro . '&fecha_transaccion=' . $fecha_actual);
        $validar = procesarResponseApiJsonToArray($api_valida);
        $catalogo_caja = $this->curl->curlGet('api/catalogo-caja');
        $data['cat_cajas'] = procesarResponseApiJsonToArray($catalogo_caja);
        if (!isset($validar->id)) {
            $data['titulo'] = "Caja";
            $data['subtitulo'] = "Corte de caja";
            $cuentas = $this->curl->curlGet('api/cuentas-por-cobrar/getCierreCaja?fecha_inicio=' . $fecha_actual);

            $data['cuentas'] = procesarResponseApiJsonToArray($cuentas);
            $data['fecha_transaccion'] = $fecha_actual;
            $data['usuario_registro'] = $usuario_registro;
            $this->blade->render('reportes/corte_caja', $data);
        } else {
            $api_detalle_corte = $this->curl->curlGet('api/detalle-corte-caja/getByCajaId?corte_caja_id=' . $validar->id);
            $data['detalle_corte'] = procesarResponseApiJsonToArray($api_detalle_corte);

            $data['caja'] = $validar;
            $data['titulo'] = "Caja";
            $data['subtitulo'] = "Detalle caja";
            $this->blade->render('reportes/detalle_caja', $data);
        }
    }

    public function detalle_corte()
    {

        $fecha_actual = $this->input->get('fecha_corte');
        $usuario_registro = $this->input->get('usuario_id');
        $api_valida = $this->curl->curlGet('api/corte-caja/mostrarPorUsuarioAndFecha?usuario_registro=' . $usuario_registro . '&fecha_transaccion=' . $fecha_actual);
        $validar = procesarResponseApiJsonToArray($api_valida);
        $catalogo_caja = $this->curl->curlGet('api/catalogo-caja');
        $data['cat_cajas'] = procesarResponseApiJsonToArray($catalogo_caja);

        $api_detalle_corte = $this->curl->curlGet('api/detalle-corte-caja/getByCajaId?corte_caja_id=' . $validar->id);
        $data['detalle_corte'] = procesarResponseApiJsonToArray($api_detalle_corte);

        $data['caja'] = $validar;
        $data['titulo'] = "Caja";
        $data['subtitulo'] = "Detalle caja";
        $this->blade->render('reportes/detalle_caja', $data);
    }
}
