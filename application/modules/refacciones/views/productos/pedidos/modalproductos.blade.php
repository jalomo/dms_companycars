
<div class="modal fade" id="modalcambioestatus" tabindex="-1" role="dialog" aria-labelledby="modalcambioestatus" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_cambio_estatus">Producto</h5>
            </div>
            <div class="modal-body">
                <form id="frmmodalcambioestatus" class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Estatus de producto:</label>
                            <select class="form-control" id="estatus_id" name="estatus_id" style="width: 100%;">
                                <option value="">Selecionar </option>
                                
                                <option value="3"> No pedidas </option>
                                <option value="4">Backorder </option>
                            </select>
                            <input type="hidden" name="id_pedido_producto" id="id_pedido_producto">
                            <div id="estatus_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button id="btn-confirmar-cambioestatus" type="button" class="btn btn-primary">
                    <i class="fas fa-list"></i> Aceptar
                </button>
            </div>
        </div>
    </div>
</div>