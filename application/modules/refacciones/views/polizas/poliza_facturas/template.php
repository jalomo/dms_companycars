<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .contenedor {
            width: 100%;
        }

        .col-12 {
            width: 100%;
            padding: 3px;
        }

        .col-6 {
            float: left;
            width: 49%;
            padding: 3px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        table,
        th,
        td {
            border: 1px solid #233a74;
            margin-bottom: 12px
        }
    </style>
</head>

<body>
    <div class="contenedor">
        <div class="col-12">
            <table class="table">
                <thead class="">
                    <tr>
                        <th scope="col">
                            No factura
                        </th>
                        <th scope="col">
                            Serie Certificado del Emisor
                        </th>
                        <th scope="col" style="width: 30%">
                            Fecha
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="text-align:center"><?php echo $folio->folio ?></td>
                        <td style="text-align:center"><?php echo $folio->no_certificado ?></td>
                        <td style="text-align:center">
                            <?php echo date('d/m/yy', strtotime($folio->created_at)) ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>