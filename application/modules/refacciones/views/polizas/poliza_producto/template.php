<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .contenedor {
            width: 100%;
        }

        .col-12 {
            width: 100%;
            padding: 3px;
        }

        .col-6 {
            float: left;
            width: 49%;
            padding: 3px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        table,
        th,
        td {
            border: 1px solid #233a74;
            margin-bottom: 12px
        }
    </style>
</head>

<body>
    <div class="contenedor">
        <div class="col-12">
            <table class="table">
                <thead class="">
                    <tr>
                        <th scope="col">
                            Núm. identificación
                        </th>
                        <th scope="col">
                            Descripción
                        </th>
                        <th scope="col" style="width: 20%">
                            Unidad
                        </th>
                        <th scope="col" style="width: 20%">
                            Precio
                        </th>
                        <th scope="col" style="width: 30%">
                            Fecha
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="text-align:center"><?php echo $producto->no_identificacion ?></td>
                        <td style="text-align:center"><?php echo $producto->descripcion ?></td>
                        <td style="text-align:center"><?php echo $producto->precio_factura ?></td>
                        <td style="text-align:center">
                            $ <?php echo porcentajeProducto($producto->rel_precio->precio_publico, $producto->precio_factura) ?>
                        </td>
                        <td style="text-align:center">
                            <?php echo date('d/m/yy', strtotime($producto->created_at)) ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>