@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            <div style="margin:10px;">
                <form name="" id="formulario" method="post" action="<?= base_url() . "presupuestos/Presupuestos/validateFormR2" ?>" autocomplete="on" enctype="multipart/form-data">
                    <div class="alert alert-success" align="center" style="display:none;">
                        <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                    </div>
                    <div class="alert alert-danger" align="center" style="display:none;">
                        <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                    </div>
                    <div class="alert alert-warning" align="center" style="display:none;">
                        <strong style="font-size:20px !important;">Campos incompletos.</strong>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <?php echo renderInputText("text", "noSerie", "No. Orden :", isset($id_cita) ? $id_cita : ''); ?>
                        </div>
                        <div class="col-md-3">
                            <?php echo renderInputText("text", "noSerie", "No.Serie (VIN):", isset($serie) ? $serie : ''); ?>
                        </div>
                        <div class="col-md-3">
                            <?php echo renderInputText("text", "modelo", "Modelo:", isset($modelo) ? $modelo : ''); ?>
                        </div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <?php echo renderInputText("text", "placas", "Placas:", ''); ?>
                        </div>
                        <div class="col-md-3">
                            <?php echo renderInputText("text", "unidad", "Unidad:", ''); ?>
                        </div>
                        <div class="col-md-3">
                            <?php echo renderInputText("text", "tecnico", "Técnico:", ''); ?>
                        </div>
                        <div class="col-md-3">
                            <?php echo renderInputText("text", "asesors", "Asesor:", ''); ?>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-9 text-right">
                            <label for="" class="error"> <strong>*</strong> Guardar renglon [<i class="fas fa-plus"></i>] </label>
                            <br>
                            <label for="" class="error"> <strong>*</strong> Eliminar renglon [<i class="fas fa-minus"></i>] </label>
                        </div>
                        <div class="col-md-1" align="right">
                            <a id="agregarRefaccionRef" class="btn btn-success" style="color:white;">
                                <i class="fas fa-plus"></i>
                            </a>
                        </div>
                        <div class="col-md-1" align="right">
                            <a id="eliminarRefaccionRef" class="btn btn-danger" style="color:white; width: 1cm;" disabled>
                                <i class="fas fa-minus"></i>
                            </a>
                        </div>
                    </div>
                    <?php if (isset($envia_ventanilla)) : ?>
                        <?php if ($envia_ventanilla != "0") : ?>
                            <div class="alert alert-danger" align="center">
                                <strong style="font-size:13px !important;color:darkblue;">
                                    Proceso del presupuesto: <?php echo $ubicacion_proceso; ?>
                                    <br>
                                    Proceso del presupuesto con garantías: <?php echo $ubicacion_proceso_garantia; ?>
                                </strong>
                                <br>
                                <strong style="font-size:13px !important;">
                                    Vigencia: Un mes a partir de la fecha de emisión del presente presupuesto. (Fecha de vencimiento: <?php if (isset($fecha_limite)) echo $fecha_limite; ?>)
                                </strong>
                            </div>
                        <?php endif ?>
                    <?php endif ?>

                    <div class="row">
                        <div class="col-md-12 table-responsive" style="overflow-x: scroll;width: auto;">
                            <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;min-width: 833px;margin:10px;">
                                <thead>
                                    <tr style="background-color: #eee;">
                                        <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                            CANTIDAD
                                        </td>
                                        <td style="width:30%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                            D E S C R I P C I Ó N
                                        </td>
                                        <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                            NUM. PIEZA
                                        </td>
                                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 10px;" align="center" colspan="3">
                                            EXISTE
                                        </td>
                                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                                            FECHA<br>TENTATIVA
                                        </td>
                                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                            PRECIO PÚBLICO
                                        </td>
                                        <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                            HORAS
                                        </td>
                                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                            COSTO MO
                                        </td>
                                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                            TOTAL REFACCIÓN
                                        </td>
                                        <!--<td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                            PRIORIDAD <br> REFACCION
                                        </td>-->
                                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                            PZA. GARANTÍA
                                            <input type="hidden" name="" id="indiceTablaMateria" value="<?php if (isset($renglon)) echo count($renglon) + 1;
                                                                                                        else echo "1"; ?>">
                                            <input type="hidden" name="" id="indice_limite" value="<?php if (isset($renglon)) echo count($renglon) + 1;
                                                                                                    else echo "1"; ?>">
                                        </td>
                                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">

                                        </td>
                                    </tr>
                                    <tr style="background-color: #ddd;">
                                        <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                                            SI
                                        </td>
                                        <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                                            NO
                                        </td>
                                        <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                                            PLANTA
                                        </td>
                                    </tr>
                                </thead>
                                <tbody id="cuerpoMateriales">
                                    <!-- Verificamos si existe la informacion -->
                                    <?php if (isset($renglon)) : ?>
                                        <!-- Comprobamos que haya registros guardados -->
                                        <?php if (count($renglon) > 0) : ?>
                                            <?php for ($index  = 0; $index < count($renglon); $index++) : ?>
                                                <tr id='fila_<?php echo $index + 1; ?>'>
                                                    <!-- CANTIDAD -->
                                                    <td style='border:1px solid #337ab7;' align='center'>
                                                        <input type='number' step='any' min='0' class='input_field' onblur='actualizaValores(<?php echo $index + 1; ?>)' id='cantidad_<?php echo $index + 1; ?>' value='<?php echo $cantidad[$index]; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                                                    </td>
                                                    <!-- D E S C R I P C I Ó N -->
                                                    <td style='border:1px solid #337ab7;' align='center'>
                                                        <textarea rows='1' class='input_field_lg' onblur='actualizaValores(<?php echo $index + 1; ?>)' id='descripcion_<?php echo $index + 1; ?>' style='width:90%;text-align:left;'><?php echo $descripcion[$index]; ?></textarea>
                                                    </td>
                                                    <!-- NUM. PIEZA -->
                                                    <td style='border:1px solid #337ab7;' align='center'>
                                                        <textarea rows='1' class='input_field_lg' onblur='actualizaValores(<?php echo $index + 1; ?>)' id='pieza_<?php echo $index + 1; ?>' style='width:90%;text-align:left;'><?php echo $num_pieza[$index]; ?></textarea>
                                                    </td>
                                                    <!-- EXISTE -->
                                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                        <input type='radio' id='apuntaSi_<?php echo $index + 1; ?>' class='input_field' onclick='actualizaValores(<?php echo $index + 1; ?>)' name='existe_<?php echo $index + 1; ?>' value="SI" <?php if ($existe[$index] == "SI") echo "checked"; ?> style="transform: scale(1.5);">
                                                    </td>
                                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                        <input type='radio' id='apuntaNo_<?php echo $index + 1; ?>' class='input_field' onclick='actualizaValores(<?php echo $index + 1; ?>)' name='existe_<?php echo $index + 1; ?>' value="NO" <?php if ($existe[$index] == "NO") echo "checked"; ?> style="transform: scale(1.5);">
                                                    </td>
                                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                        <input type='radio' id='apuntaPlanta_<?php echo $index + 1; ?>' class='input_field' onclick='actualizaValores(<?php echo $index + 1; ?>)' name='existe_<?php echo $index + 1; ?>' value="PLANTA" <?php if ($existe[$index] == "PLANTA") echo "checked"; ?> style="transform: scale(1.5);">
                                                    </td>
                                                    <!-- FECHA TENTANTIVA - ENTREGA -->
                                                    <td style='border:1px solid #337ab7;' align='center'>
                                                        <input type="date" class='input_field' id="fecha_tentativa_<?php echo $index + 1; ?>" value="<?php if ($fecha_planta[$index] != "0000-00-00") echo $fecha_planta[$index];
                                                                                                                                                    else echo date('Y-m-d'); ?>" <?php if ($fecha_planta[$index] == "0000-00-00") echo "style='display: none;'"; ?>>
                                                    </td>
                                                    <!-- PRECIO PÚBLICO -->
                                                    <td style='border:1px solid #337ab7;' align='center'>
                                                        $<input type='number' step='any' min='0' class='input_field' onblur='actualizaValores(<?php echo $index + 1; ?>)' id='costoCM_<?php echo $index + 1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $costo_pieza[$index]; ?>' style='width:90%;text-align:left;'>
                                                    </td>
                                                    <!-- HORAS -->
                                                    <td style='border:1px solid #337ab7;' align='center'>
                                                        <input type='number' step='any' min='0' class='input_field' onblur='actualizaValores(<?php echo $index + 1; ?>)' id='horas_<?php echo $index + 1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $horas_mo[$index]; ?>' style='width:90%;text-align:left;'>
                                                    </td>
                                                    <!-- COSTO MO -->
                                                    <td style='border:1px solid #337ab7;' align='center'>
                                                        $<input type='number' step='any' min='0' id='totalReng_<?php echo $index + 1; ?>' onblur="actualizaValores(<?php echo $index + 1; ?>)" class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $costo_mo[$index]; ?>' style='width:90%;text-align:left;' disabled>

                                                        <!-- total de la refaccion -->
                                                        <input type='hidden' id='totalOperacion_<?php echo $index + 1; ?>' value='<?php echo $total_refacion[$index]; ?>'>

                                                        <!-- Indice interno de la recaccion -->
                                                        <input type='hidden' id='id_refaccion_<?php echo $index + 1; ?>' name='id_registros[]' value="<?php echo $id_refaccion[$index]; ?>">
                                                        <!-- Informacion del renglon -->
                                                        <input type='hidden' id='valoresCM_<?php echo $index + 1; ?>' name='registros[]' value="<?php echo $renglon[$index]; ?>">
                                                        <!-- Informacion de datos de garantia de la refaccion (si aplica) -->
                                                        <input type='hidden' id='valoresGarantias_<?php echo $index + 1; ?>' name='registrosGarantias[]' value="<?php echo $renglonGarantia[$index]; ?>">

                                                        <!-- Informacion del aprueba jefe de taller -->
                                                        <input type='hidden' id='aprueba_jdt_<?php echo $index + 1; ?>' value="<?php echo $autoriza_jefeTaller[$index]; ?>">
                                                        <!-- Informacion pieza con garantia -->
                                                        <input type='hidden' name="garantia[]" id='garantia_<?php echo $index + 1; ?>' value="<?php echo $pza_garantia[$index]; ?>">
                                                        <!-- Informacion de prioridad de la refaccion -->
                                                        <input type="hidden" name="prioridad_<?php echo $index + 1; ?>" id="prioridad_<?php echo $index + 1; ?>" value="<?php if (isset($prioridad[$index])) echo $prioridad[$index];
                                                                                                                                                                    else echo "1"; ?>">
                                                    </td>
                                                    <!-- TOTAL REFACCIÓN -->
                                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                        <label id="total_renglon_label_<?php echo $index + 1; ?>" style="color:darkblue;">$<?php echo number_format($total_refacion[$index], 2); ?></label>
                                                    </td>
                                                    <!-- PZA. GARANTÍA -->
                                                    <td style='border:1px solid #337ab7;' align='center'>
                                                        <input type='checkbox' class='input_field' onclick='refaccionGarantia(<?php echo $index + 1; ?>)' name="pzaGarantia[]" value="<?php echo $index + 1; ?>" id='garantia_check_<?php echo $index + 1; ?>' <?php if ($pza_garantia[$index] == "1") echo "checked"; ?> style="transform: scale(1.5);" disabled>
                                                    </td>
                                                    <!-- BUSCAR REFACCION EN INVENTARIO -->
                                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                        <!--<a id='refaccionBusqueda_<?php echo $index + 1; ?>' class='refaccionesBox btn btn-warning' data-id="<?php echo $index + 1; ?>" data-target="#refacciones" data-toggle="modal" style="color:white;">
                                                            <i class="fa fa-search"></i>
                                                        </a>-->
                                                    </td>
                                                </tr>
                                            <?php endfor; ?>

                                            <!-- Creamos un renglon vacio por si se va a agregar algun item -->
                                            <tr id='fila_<?php echo count($renglon) + 1; ?>'>
                                                <!-- CANTIDAD -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <input type='number' step='any' min='0' class='input_field' onblur='actualizaValores(<?php echo count($renglon) + 1; ?>)' id='cantidad_<?php echo count($renglon) + 1; ?>' value='1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                                                </td>
                                                <!-- D E S C R I P C I Ó N -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <textarea rows='1' class='input_field_lg' onblur='actualizaValores(<?php echo count($renglon) + 1; ?>)' id='descripcion_<?php echo count($renglon) + 1; ?>' style='width:90%;text-align:left;'></textarea>
                                                </td>
                                                <!-- NUM. PIEZA -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <textarea rows='1' class='input_field_lg' onblur='actualizaValores(<?php echo count($renglon) + 1; ?>)' id='pieza_<?php echo count($renglon) + 1; ?>' style='width:90%;text-align:left;'></textarea>
                                                </td>
                                                <!-- EXISTE -->
                                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                    <input type='radio' id='apuntaSi_<?php echo count($renglon) + 1; ?>' class='input_field' onclick='actualizaValores(<?php echo count($renglon) + 1; ?>)' name='existe_<?php echo count($renglon) + 1; ?>' value="SI" style="transform: scale(1.5);">
                                                </td>
                                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                    <input type='radio' id='apuntaNo_<?php echo count($renglon) + 1; ?>' class='input_field' onclick='actualizaValores(<?php echo count($renglon) + 1; ?>)' name='existe_<?php echo count($renglon) + 1; ?>' value="NO" style="transform: scale(1.5);">
                                                </td>
                                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                    <input type='radio' id='apuntaPlanta_<?php echo count($renglon) + 1; ?>' class='input_field' onclick='actualizaValores(<?php echo count($renglon) + 1; ?>)' name='existe_<?php echo count($renglon) + 1; ?>' value="PLANTA" style="transform: scale(1.5);">
                                                </td>
                                                <!-- FECHA TENTANTIVA - ENTREGA -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <input type="date" class='input_field' min="<?php echo date('Y-m-d'); ?>" id="fecha_tentativa_<?php echo count($renglon) + 1; ?>" value="<?php echo date('Y-m-d'); ?>" name="" style="display: none;">
                                                </td>
                                                <!-- PRECIO PÚBLICO -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    $<input type='number' step='any' min='0' class='input_field' onblur='actualizaValores(<?php echo count($renglon) + 1; ?>)' id='costoCM_<?php echo count($renglon) + 1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                                                </td>
                                                <!-- HORAS -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <input type='number' step='any' min='0' class='input_field' onblur='actualizaValores(<?php echo count($renglon) + 1; ?>)' id='horas_<?php echo count($renglon) + 1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                                                </td>
                                                <!-- COSTO MO -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    $<input type='number' step='any' min='0' id='totalReng_<?php echo $index + 1; ?>' onblur="actualizaValores(<?php echo count($renglon) + 1; ?>)" class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='550' style='width:90%;text-align:left;' disabled>

                                                    <!-- total de la refaccion -->
                                                    <input type='hidden' id='totalOperacion_<?php echo count($renglon) + 1; ?>' value='0'>

                                                    <!-- Indice interno de la recaccion -->
                                                    <input type='hidden' id='id_refaccion_<?php echo count($renglon) + 1; ?>' name='id_registros[]' value="0">
                                                    <!-- Informacion del renglon -->
                                                    <input type='hidden' id='valoresCM_<?php echo count($renglon) + 1; ?>' name='registros[]' value="">
                                                    <!-- Informacion de datos de garantia de la refaccion (si aplica) -->
                                                    <input type='hidden' id='valoresGarantias_<?php echo count($renglon) + 1; ?>' name='registrosGarantias[]' value="0_0_0">

                                                    <!-- Informacion del aprueba jefe de taller -->
                                                    <input type='hidden' id='aprueba_jdt_<?php echo count($renglon) + 1; ?>' value="0">
                                                    <!-- Informacion pieza con garantia -->
                                                    <input type='hidden' name="garantia[]" id='garantia_<?php echo count($renglon) + 1; ?>' value="0">
                                                    <!-- Informacion de prioridad de la refaccion -->
                                                    <input type="hidden" name="prioridad_<?php echo count($renglon) + 1; ?>" id="prioridad_<?php echo count($renglon) + 1; ?>" value="2">
                                                </td>
                                                <!-- TOTAL REFACCIÓN -->
                                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                    <label id="total_renglon_label_<?php echo count($renglon) + 1; ?>" style="color:darkblue;">$0.00</label>
                                                </td>
                                                <!-- PRIORIDAD <br> REFACCION -->
                                                <!--<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                    <input type='number' step='any' min='0' class='input_field' id='prioridad_<?php echo count($renglon) + 1; ?>' name='prioridad_<?php echo count($renglon) + 1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='2' style='width:70%;text-align:center;'>
                                                </td>-->
                                                <!-- PZA. GARANTÍA -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <input type='checkbox' class='input_field' name='pzaGarantia[]' onclick='refaccionGarantia(<?php echo count($renglon) + 1; ?>)' value='<?php echo count($renglon) + 1; ?>' id='garantia_check_<?php echo count($renglon) + 1; ?>' style='transform: scale(1.5);'>
                                                </td>
                                                <!-- BUSCAR REFACCION EN INVENTARIO -->
                                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                    <!--<a id='refaccionBusqueda_<?php echo count($renglon) + 1; ?>' class='refaccionesBox btn btn-warning' data-id="<?php echo count($renglon) + 1; ?>" data-target="#refacciones" data-toggle="modal" style="color:white;">
                                                        <i class="fa fa-search"></i>
                                                    </a>-->
                                                </td>
                                            </tr>
                                        <?php else : ?>
                                            <tr>
                                                <td colspan="13" align="center">
                                                    No se cargaron los datos
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php else : ?>
                                        <tr>
                                            <td colspan="13" align="center">
                                                No se cargaron los datos
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="10" align="right">
                                            <label>SUB-TOTAL</label>
                                        </td>
                                        <td colspan="1" align="center">
                                            $<label for="" id="subTotalMaterialLabel"><?php if (isset($subtotal)) echo number_format($subtotal, 2);
                                                                                        else echo "0"; ?></label>
                                            <input type="hidden" name="subTotalMaterial" id="subTotalMaterial" value="<?php if (isset($subtotal)) echo $subtotal;
                                                                                                                        else echo "0"; ?>">
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="10" align="right" class="error">
                                            <label>CANCELADO</label>
                                        </td>
                                        <td colspan="1" align="center" class="error">
                                            $<label id="canceladoMaterialLabel"><?php if (isset($cancelado)) echo number_format($cancelado, 2);
                                                                                else echo "0"; ?></label>
                                            <input type="hidden" name="canceladoMaterial" id="canceladoMaterial" value="<?php if (isset($cancelado)) echo $cancelado;
                                                                                                                        else echo "0"; ?>">
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="10" align="right">
                                            <label>I.V.A.</label>
                                        </td>
                                        <td colspan="1" align="center">
                                            $<label for="" id="ivaMaterialLabel"><?php if (isset($iva)) echo number_format($iva, 2);
                                                                                    else echo "0"; ?></label>
                                            <input type="hidden" name="ivaMaterial" id="ivaMaterial" value="<?php if (isset($iva)) echo $iva;
                                                                                                            else echo "0"; ?>">
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="10" align="right">
                                            <label>PRESUPUESTO TOTAL</label>
                                        </td>
                                        <td colspan="1" align="center">
                                            $<label id="presupuestoMaterialLabel"><?php if (isset($subtotal)) echo number_format((($subtotal + $cancelado) * 1.16), 2);
                                                                                    else echo "0"; ?></label>
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr style="border-top:1px solid #337ab7;">
                                        <td colspan="8" rowspan="2">
                                            <label for="" id="anticipoNota" style="color:blue;">Nota anticipo: <?php if (isset($notaAnticipo)) echo $notaAnticipo;
                                                                                                                else echo ""; ?></label>
                                            <input type="hidden" name="anticipoNota" value="<?php if (isset($notaAnticipo)) echo $notaAnticipo;
                                                                                            else echo ""; ?>">
                                        </td>
                                        <td colspan="2" align="right">
                                            <label>ANTICIPO</label>
                                        </td>
                                        <td align="center">
                                            $<label for="" id="anticipoMaterialLabel"><?php if (isset($anticipo)) echo number_format($anticipo, 2);
                                                                                        else echo "0"; ?></label>
                                            <input type="hidden" name="anticipoMaterial" id="anticipoMaterial" value="<?php if (isset($anticipo)) echo $anticipo;
                                                                                                                        else echo "0"; ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            <label>TOTAL</label>
                                        </td>
                                        <td colspan="1" align="center">
                                            $<label for="" id="totalMaterialLabel"><?php if (isset($total)) echo number_format($total, 2);
                                                                                    else echo "0"; ?></label>
                                            <input type="hidden" name="totalMaterial" id="totalMaterial" value="<?php if (isset($total)) echo $total;
                                                                                                                else echo "0"; ?>">
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="10" align="right">
                                            FIRMA DEL ASESOR QUE APRUEBA
                                        </td>
                                        <td align="center" colspan="3">
                                            <img class='marcoImg' src='<?php echo base_url(); ?><?php if (isset($firma_asesor)) {
                                                                                                    echo (($firma_asesor != "") ?  $firma_asesor : "assets/imgs/fondo_bco.jpeg");
                                                                                                } else {
                                                                                                    echo "assets/imgs/fondo_bco.jpeg";
                                                                                                } ?>' id='' style='width:80px;height:30px;'>
                                            <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value='<?php if (isset($firma_asesor)) echo $firma_asesor ?>'>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                    <!-- Tabla de refacciones con garantias -->
                    <div class="row">
                        <div class="col-md-12 table-responsive" style="overflow-x: scroll;width: auto;">
                            <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;min-width: 833px;margin:10px;">
                                <thead>
                                    <tr style="background-color: #eee;border-bottom: 1px solid darkblue;">
                                        <td colspan="13" align="center">
                                            <h5 style="text-align: center;">REFACCIONES CON GARANTIAS</h5>
                                        </td>
                                    </tr>
                                    <tr style="background-color: #eee;">
                                        <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                            CANTIDAD
                                        </td>
                                        <td style="width:30%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                            D E S C R I P C I Ó N
                                        </td>
                                        <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                            NUM. PIEZA
                                        </td>
                                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                            COSTO PZA FORD
                                        </td>
                                        <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                            HORAS
                                        </td>
                                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                            COSTO MO GARANTÍAS
                                        </td>
                                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                            TOTAL REFACCIÓN
                                        </td>
                                    </tr>
                                </thead>
                                <tbody id="cuerpoMaterialesGarantias">
                                    <!-- Verificamos que haya informacion cargada -->
                                    <?php if (isset($renglon)) : ?>
                                        <!-- Verififamos que haya refacciones con garantias -->
                                        <?php if ($registrosGarantias > 0) : ?>
                                            <?php $totalGarantia = 0; ?>
                                            <!-- Recorremos los registros para imprimir los que lleven refacciones -->
                                            <?php for ($i  = 0; $i < count($renglon); $i++) : ?>
                                                <!-- Comprobamos que la refaccion tenga garnatia -->
                                                <?php if ($pza_garantia[$i] == "1") : ?>
                                                    <tr id='tb2_fila_<?php echo $i + 1; ?>'>
                                                        <!-- CANTIDAD -->
                                                        <td style='border:1px solid #337ab7;' align='center'>
                                                            <input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresGarantias(<?php echo $i + 1; ?>)' id='tb2_cantidad_<?php echo $i + 1; ?>' value='<?php echo $cantidad[$i]; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;' disabled>
                                                        </td>
                                                        <!-- D E S C R I P C I Ó N -->
                                                        <td style='border:1px solid #337ab7;' align='center'>
                                                            <textarea rows='1' class='input_field_lg' onblur='actualizaValoresGarantias(<?php echo $i + 1; ?>)' id='tb2_descripcion_<?php echo $i + 1; ?>' style='width:90%;text-align:left;' disabled><?php echo $descripcion[$i]; ?></textarea>
                                                        </td>
                                                        <!-- NUM. PIEZA -->
                                                        <td style='border:1px solid #337ab7;' align='center'>
                                                            <textarea rows='1' class='input_field_lg' onblur='actualizaValoresGarantias(<?php echo $i + 1; ?>)' id='tb2_pieza_<?php echo $i + 1; ?>' style='width:90%;text-align:left;' disabled><?php echo $num_pieza[$i]; ?></textarea>
                                                        </td>
                                                        <!-- COSTO PZA FORD -->
                                                        <td style='border:1px solid #337ab7;' align='center'>
                                                            $<input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresGarantias(<?php echo $i + 1; ?>)' id='tb2_costoCM_<?php echo $i + 1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $costo_ford[$i]; ?>' style='width:90%;text-align:left;'>
                                                        </td>
                                                        <!-- HORAS -->
                                                        <td style='border:1px solid #337ab7;' align='center'>
                                                            <input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresGarantias(<?php echo $i + 1; ?>)' id='tb2_horas_<?php echo $i + 1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $horas_mo_garantias[$i]; ?>' style='width:90%;text-align:left;'>
                                                        </td>
                                                        <!-- COSTO MO GARANTÍAS -->
                                                        <td style='border:1px solid #337ab7;' align='center'>
                                                            $<input type='number' step='any' min='0' id='tb2_totalReng_<?php echo $i + 1; ?>' onblur="actualizaValoresGarantias(<?php echo $i + 1; ?>)" class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $costo_mo_garantias[$i]; ?>' style='width:90%;text-align:left;'>

                                                        </td>
                                                        <!-- TOTAL REFACCIÓN -->
                                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                            <!-- Calculamos el total para visualizar -->
                                                            <?php $total = ((float) $cantidad[$i] * (float) $costo_ford[$i]) + ((float) $horas_mo_garantias[$i] * (float) $costo_mo_garantias[$i]); ?>
                                                            <?php $totalGarantia += $total; ?>
                                                            <label id="tb2_renglon_label_<?php echo $i + 1; ?>" style="color:darkblue;">$<?php echo number_format($total, 2); ?></label>
                                                        </td>
                                                    </tr>
                                                <?php endif ?>
                                            <?php endfor ?>
                                        <?php else : ?>
                                            <tr>
                                                <td colspan="7" style="border-bottom:1px solid #337ab7;">
                                                    <h5 style="text-align: center">Sin refacciones para mostrar</h5>
                                                </td>
                                            </tr>
                                        <?php endif ?>
                                    <?php else : ?>
                                        <tr>
                                            <td colspan="7" style="border-bottom:1px solid #337ab7;">
                                                <h5 style="text-align: center">Sin refacciones para mostrar</h5>
                                            </td>
                                        </tr>
                                    <?php endif ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="6" align="right">
                                            SUB-TOTAL
                                        </td>
                                        <td colspan="1" align="center">
                                            $<label for="" id="subTotalMaterialLabel_tabla2"><?php if (isset($totalGarantia)) echo $totalGarantia; ?></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" align="right">
                                            I.V.A.
                                        </td>
                                        <td colspan="1" align="center">
                                            $<label for="" id="ivaMaterialLabel_tabla2"><?php if (isset($totalGarantia)) echo number_format(($totalGarantia * 0.16)); ?></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" align="right">
                                            TOTAL
                                        </td>
                                        <td colspan="1" align="center">
                                            $<label for="" id="totalMaterialLabel_tabla2"><?php if (isset($totalGarantia)) echo number_format(($totalGarantia * 1.16)); ?></label>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                    <br>
                    <!-- Comentarios del asesor -->
                    <div class="row">
                        <div class="col-sm-5">
                            <h4>Notas del asesor:</h4>
                            <!-- Verificamos si es una vista del asesor -->
                            <label for="" style="font-size:12px;"><?php if (isset($nota_asesor)) {
                                                                        if ($nota_asesor != "") echo $nota_asesor;
                                                                        else echo "Sin comentarios";
                                                                    } else echo "Sin comentarios"; ?></label>
                        </div>
                        <div class="col-sm-7" align="right">
                            <h4>Archivo(s) de la cotización:</h4>
                            <br>

                            <?php if (isset($archivos_presupuesto)) : ?>
                                <?php $contador = 1; ?>
                                <?php for ($i = 0; $i < count($archivos_presupuesto); $i++) : ?>
                                    <?php if ($archivos_presupuesto[$i] != "") : ?>
                                        <a href="<?php echo base_url() . $archivos_presupuesto[$i]; ?>" class="btn btn-info" style="font-size: 9px;font-weight: bold;" target="_blank">
                                            DOC. (<?php echo $contador; ?>)
                                        </a>

                                        <?php if ((($i + 1) % 4) == 0) : ?>
                                            <br>
                                        <?php endif; ?>

                                        <?php $contador++; ?>
                                    <?php endif; ?>
                                <?php endfor; ?>

                                <?php if (count($archivos_presupuesto) == 0) : ?>
                                    <h4 style="text-align: right;">Sin archivos</h4>
                                    <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                                <?php endif; ?>
                            <?php endif; ?>

                            <hr>
                            <!-- Para agregar mas documentos -->
                            <input type="file" multiple name="uploadfiles[]">
                            <input type="hidden" name="uploadfiles_resp" value="<?php if (isset($archivos_presupuesto_lineal)) echo $archivos_presupuesto_lineal; ?>">
                            <br>
                        </div>
                    </div>

                    <hr>
                    <!-- Comentarios del técnico -->
                    <div class="row">
                        <div class="col-md-6">
                            <h4>Comentario del técnico para Refacciones:</h4>
                            <label for="" style="font-size:12px;"><?php if (isset($comentario_tecnico)) {
                                                                        if ($comentario_tecnico != "") echo $comentario_tecnico;
                                                                        else echo "Sin comentarios";
                                                                    } else echo "Sin comentarios"; ?></label>
                        </div>

                        <div class="col-sm-6" align="right">
                            <h4>Archivo(s) del técnico para Refacciones:</h4>

                            <?php if (isset($archivo_tecnico)) : ?>
                                <?php $contador = 1; ?>
                                <?php for ($i = 0; $i < count($archivo_tecnico); $i++) : ?>
                                    <?php if ($archivo_tecnico[$i] != "") : ?>
                                        <a href="<?php echo base_url() . $archivo_tecnico[$i]; ?>" class="btn" style="background-color: #5cb8a5;border-color: #4c8bae;color:white;font-size: 9px;font-weight: bold;" target="_blank">
                                            DOC. (<?php echo $contador; ?>)
                                        </a>

                                        <?php if (($contador % 4) == 0) : ?>
                                            <br>
                                        <?php endif; ?>

                                        <?php $contador++; ?>
                                    <?php endif; ?>
                                <?php endfor; ?>

                                <?php if (count($archivo_tecnico) == 0) : ?>
                                    <h4 style="text-align: right;">Sin archivos</h4>
                                    <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>

                    <hr>
                    <!-- comentarios de ventanilla -->
                    <div class="row">
                        <div class="col-sm-6" align="left">
                            <h4>Comentarios de Ventanilla:</h4>
                            <textarea id="comentario_ventanilla" name="comentario_ventanilla" rows="2" style="width:100%;border-radius:4px;" placeholder="Notas de ventanilla..."><?php if (isset($comentario_ventanilla)) echo $comentario_ventanilla; ?></textarea>
                        </div>
                        <div class="col-sm-6" align="right"></div>
                    </div>

                    <hr>
                    <!-- comentarios del jefe de taller -->
                    <div class="row">
                        <div class="col-sm-6" align="left">
                            <h4>Comentarios del Jefe de Taller:</h4>
                            <label for="" style="font-size:12px;"><?php if (isset($comentario_jdt)) {
                                                                        if ($comentario_jdt != "") echo $comentario_jdt;
                                                                        else echo "Sin comentarios";
                                                                    } else echo "Sin comentarios"; ?></label>
                        </div>
                        <div class="col-sm-6" align="right">
                            <h4>Archivo(s) del Jefe de Taller:</h4>

                            <?php if (isset($archivo_jdt)) : ?>
                                <?php $contador = 1; ?>
                                <?php for ($i = 0; $i < count($archivo_jdt); $i++) : ?>
                                    <?php if ($archivo_jdt[$i] != "") : ?>
                                        <a href="<?php echo base_url() . $archivo_jdt[$i]; ?>" class="btn btn-primary" style="font-size: 9px;font-weight: bold;" target="_blank">
                                            DOC. (<?php echo $contador; ?>)
                                        </a>

                                        <?php if (($contador % 4) == 0) : ?>
                                            <br>
                                        <?php endif; ?>

                                        <?php $contador++; ?>
                                    <?php endif; ?>
                                <?php endfor; ?>

                                <?php if (count($archivo_jdt) == 0) : ?>
                                    <h4 style="text-align: right;">Sin archivos</h4>
                                    <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>

                    <hr>
                    <!-- comentarios de ventanilla -->
                    <div class="row">
                        <div class="col-sm-6" align="left">
                            <h4>Comentarios de Garantías:</h4>
                            <label for="" style="font-size:12px;"><?php if (isset($comentario_garantia)) {
                                                                        if ($comentario_garantia != "") echo $comentario_garantia;
                                                                        else echo "Sin comentarios";
                                                                    } else echo "Sin comentarios"; ?></label>
                        </div>
                        <div class="col-sm-6" align="right"></div>
                    </div>

                    <br>
                    <div class="col-sm-12" align="center">
                        <!-- Datos basicos -->
                        <input type="hidden" name="dirige" id="dirige" value="<?php if (isset($dirige)) echo $dirige; ?>">
                        <input type="hidden" name="modoVistaFormulario" id="modoVistaFormulario" value="3">
                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if (isset($mensaje)) echo $mensaje; ?>">
                        <input type="hidden" name="" id="usuarioActivo" value="<?php if ($this->session->userdata('usuario')) echo $this->session->userdata('usuario');
                                                                                else echo "Sin sesión"; ?>">
                        <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if (isset($UnidadEntrega)) echo $UnidadEntrega;
                                                                                            else echo "0"; ?>">

                        <!-- validamos los envios de cada rol -->
                        <input type="hidden" name="envioRefacciones" id="envioRefacciones" value="<?php if (isset($envia_ventanilla)) echo $envia_ventanilla;
                                                                                                    else echo "0"; ?>">
                        <input type="hidden" name="" id="envioJefeTaller" value="<?php if (isset($envio_jdt)) echo $envio_jdt;
                                                                                    else echo "0"; ?>">
                        <input type="hidden" name="" id="envioGarantias" value="<?php if (isset($envio_garantia)) echo $envio_garantia;
                                                                                else echo "0"; ?>">

                        <button type="button" class="btn btn-dark" onclick="location.href='<?= base_url() . "Listar_Ventanilla/Ventanilla"; ?>';">
                            Regresar
                        </button>

                        <!--<button type="submit" class="btn btn-success" name="" id="terminarCotizacion" >Actualizar Cotizacion Multipunto</button>
                        <button type="submit" class="btn btn-success" name="" id="terminarCotizacion2" >Termina Revisión</button> -->
                        <input type="submit" class="btn btn-success terminarCotizacion" style="margin-left: 20px;" name="aceptarCotizacion" value="Actualiar Presupuesto">
                        <input type="submit" class="btn terminarCotizacion" style="background-color: #1f711f;color: white;margin-left: 20px;" name="aceptarCotizacion" value="Enviar Presupuesto">

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection