@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Lista</li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_productos" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Num. Orden</th>
                            <th>Provedor</th>
                            <th>factura</th>
                            <th>Tipo</th>
                            <th>fecha</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach ($listado as $item)    
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->orden_entrada }}</td>
                                    <td>{{ $item->proveedor_id }}</td>
                                    <td>
                                        {{ $item->factura }}
                                    </td>
                                    <td>{{ $item->tipo }}</td>
                                    <td>{{ $item->fecha }}</td>
                                    <td>
                                        <a class="btn btn-success" href="{{ site_url('refacciones/almacenes/productoRemplazo/'.$item->id) }}"> 
                                            <i class="fas fa-list"></i> </a>
                                    </td>
                                </tr>
                            @endforeach
                           
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Num. Orden</th>
                            <th>Provedor</th>
                            <th>factura</th>
                            <th>Tipo</th>
                            <th>fecha</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#tbl_productos').DataTable({})
    });

    $("#tbl_productos").on("click", ".btn-borrar", function() {
        var id = $(this).data('id')
        borrar(id)
    });

    function borrar(id) {
        utils.displayWarningDialog("Desea borrar el registro??", "warning", function(data) {
            if (data.value) {
                ajax.delete(`/api/productos/${id}`, null, function(response, headers) {
                    if (headers.status != 204) {
                        return utils.displayWarningDialog(headers.message)
                    }
                    location.reload(true)
                })

            }
        }, true)
    }

</script>
<script type="text/javascript">
        $(document).ready(function(){
            $("#menu_refacciones").addClass("show");
            $("#refacciones_salidas").addClass("show");
            $("#refacciones_salidas").addClass("active");
            $("#refacciones_salidas_sub").addClass("show");
            $("#refacciones_salidas_sub").addClass("active");
            $("#dev_a_prov").addClass("active");
            $("#M02").addClass("active");
        });
</script>
@endsection