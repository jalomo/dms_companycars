@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
  <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
  <ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
  </ol>

  <div class="row">
    <div class="col-md-12">
      <form>
        <div class="form-group">
          <label for="producto_id_sat">Codigo Almacén</label>
          <input type="text" class="form-control" value="<?php if (isset($data->no_identificacion)) {
                                                            print_r($data->no_identificacion);
                                                          } ?>" id="no_identificacion" name="no_identificacion" placeholder="No identificacion">
          <div id="no_identificacion_error" class="invalid-feedback"></div>
        </div>
        <div class="form-group">
          <label for="producto_id_sat">Nombre</label>
          <input type="text" class="form-control" value="<?php echo isset($data->clave_unidad) ? $data->clave_unidad : ""; ?>" id="clave_unidad" name="clave_unidad" placeholder="Clave unidad">
          <div id="clave_unidad_error" class="invalid-feedback"></div>
        </div>
        <div class="form-group">
          <label for="producto_id_sat">Responsable</label>
          <input type="text" class="form-control" value="<?php if (isset($data->clave_prod_serv)) {
                                                            print_r($data->clave_prod_serv);
                                                          } ?>" id="clave_prod_serv" name="clave_prod_serv" placeholder="Id sat">
          <div id="clave_prod_serv_error" class="invalid-feedback"></div>
        </div>
        <div class="form-group">
          <label for="descripcion">Ubicación</label>
          <input type="text" class="form-control" value="<?php if (isset($data->descripcion)) {
                                                            print_r($data->descripcion);
                                                          } ?>" id="descripcion" name="descripcion" placeholder="Id sat">
          <div id="descripcion_error" class="invalid-feedback"></div>
        </div>
        <center>
          <button type="button" id="mostrar_mapa" onclick="hidemap()" class="btn btn-primary">Mostrar mapa</button>
          <br>
          <br>
          <div id="mapa">
            <div id="map-9cd199b9cc5410cd3b1ad21cab2e54d3"></div>
            <script>
              (function() {
                var setting = {
                  "height": 400,
                  "width": 1000,
                  "zoom": 13,
                  "queryString": "Colima, Col., México",
                  "place_id": "ChIJF6JVNFJFJYQR5lX1WvyXtLY",
                  "satellite": true,
                  "centerCoord": [19.240052631046808, -103.7286077],
                  "cid": "0xb6b497fc5af555e6",
                  "lang": "es",
                  "cityUrl": "/mexico/colima-2059",
                  "cityAnchorText": "Mapa de Colima, Colima, México",
                  "id": "map-9cd199b9cc5410cd3b1ad21cab2e54d3",
                  "embed_id": "180359"
                };
                var d = document;
                var s = d.createElement('script');
                s.src = 'https://embedgooglemap.1map.com/js/script-for-user.js?embed_id=180359';
                s.async = true;
                s.onload = function(e) {
                  window.OneMap.initMap(setting)
                };
                var to = d.getElementsByTagName('script')[0];
                to.parentNode.insertBefore(s, to);
              })();
            </script><a href="https://embedgooglemap.1map.com/es?embed_id=180359">1 Map</a>
          </div>
        </center>


        <div class="form-check">
          <input type="checkbox" checked class="form-check-input" id="status">
          <label class="form-check-label" for="status">Status</label>
        </div>

        <br>
        <button type="button" id="guardar_almacén" class="btn btn-primary">Guardar</button>

        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;"></div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  $(document).ready(function() {
    var x = document.getElementById("mapa");
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  });

  function hidemap() {
    var x = document.getElementById("mapa");
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }



  $("#guardar_producto").on('click', function() {
    if ($('#inventariable').is(":checked")) {
      inventariable = 1;
    } else {
      inventariable = 0;
    }
    if ($('#existencia').is(":checked")) {
      existencia = 1;
    } else {
      existencia = 0;
    }

    $(".invalid-feedback").html("");
    let data = procesarForm();
    ajax.post(`/api/productos/`, data, function(response, headers) {
      if (!headers.status === 400) {
        ajax.showValidations(headers);
      } else {
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Operación realizada',
          text: headers.message,
          showConfirmButton: false,
          timer: 2000
        })
        setTimeout(function() {
          return window.location.href = base_url;
        }, 1000);
        console.log(headers);
      }
      //  return window.location.href = base_url;
    })

  })

  let procesarForm = function() {
    let form = $('#registro_producto').serializeArray();
    let newArray = {
      no_identificacion: document.getElementById("no_identificacion").value,
      clave_unidad: document.getElementById("clave_unidad").value,
      clave_prod_serv: document.getElementById("clave_prod_serv").value,
      descripcion: document.getElementById("descripcion").value,
      unidad: document.getElementById("unidad").value,
      cantidad: document.getElementById("cantidad").value,
      precio: document.getElementById("precio").value,
      inventariable,
      existencia,

    };
    console.log(newArray);
    return newArray;

  }
</script>
@endsection