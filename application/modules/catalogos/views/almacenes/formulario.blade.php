@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="form-almacen" data-id="<?php echo isset($data->id) ? $data->id : ''?>" method="post">
                <h3>Almacen</h3>
                <?php echo renderInputText("text", "nombre", "Nombre", isset($data->nombre) ? $data->nombre : '', false); ?>
                <?php echo renderInputText("text", "ubicacion", "Ubicacion", isset($data->ubicacion) ? $data->ubicacion : '', false); ?>
                <?php echo renderInputText("text", "codigo_almacen", "Codigo de almacen", isset($data->codigo_almacen) ? $data->codigo_almacen : '', false); ?>
                @if (isset($data->id))
                <button id="btn-actualizar" class="btn btn-primary" type="button">Actualizar</button>
                    @else
                <button id="btn-guardar" class="btn btn-primary" type="button">Guardar</button>
                @endif
            </form>
            <hr>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
   
    $("#btn-guardar").on('click', function() {
        $(".invalid-feedback").html("");
        ajax.post(`/api/almacen/`,form(), function(response, headers) {
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            }

            utils.displayWarningDialog(headers.message,"success",function(data){
                return window.location.href = base_url + `catalogos/almacenesController`;
            })
        })
    })

    $("#btn-actualizar").on('click', function() {
        $(".invalid-feedback").html("");
        var id = $("#form-almacen").data('id');
       
        ajax.put(`/api/almacen/${id}`,form(), function(response, headers) {
           if (headers.status == 400) {
               return ajax.showValidations(headers);
           }

           utils.displayWarningDialog(headers.message,"success",function(data){
               return window.location.href = base_url + `catalogos/almacenesController`;
           })
        })

   })

    let form = function() {
         return  {
            nombre: document.getElementById("nombre").value,
            ubicacion: document.getElementById("ubicacion").value,
            codigo_almacen: document.getElementById("codigo_almacen").value,
        };
    }


   
</script>
@endsection