@layout('tema_luna/layout')
@section('contenido')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row mb-3">
        <div class="col-md-8"></div>
        <div class="col-md-4" align="right">
            <a class="btn btn-primary" href="<?php echo base_url('catalogos/UbicacionProductosController/crear') ?>"><i class="fa fa-plus"></i> Agregar</a>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_ubicacion_producto" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Ubicación</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Ubicación</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
	<script>
		$(document).ready(function() {
            try {
                var tabla_clientes = $('#tbl_ubicacion_producto').DataTable({
                    ajax: base_url + "catalogos/UbicacionProductosController/ajax_catalogo",
                    columns: [{
                            'data': function(data) {
                                return data.id
                            }
                        },
                        {
                            'data': function(data) {
                                return data.nombre
                            }
                        },
                        {
                            'data': function(data) {
                                btn_editar = "<a href='" + site_url + 'catalogos/UbicacionProductosController/editar/' + data.id + "' class='btn btn-success'><i class='fas fa-pen'></i></a>";
                                btn_eliminar = "<a href='#' class='btn btn-danger'><i class='far fa-trash-alt'></i></a>";
                                return btn_editar + ' ' + btn_eliminar;
                            }
                        }
                    ]
                });
            }
            catch(err) {
                console.log(err);
            }
            
        });
	</script>
@endsection