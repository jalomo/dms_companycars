@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>
    <div class="row mb-4">
        <div class="col-md-10"></div>
        <div class="col-md-2">
            <a href="{{ base_url('catalogos/preciosController/crear/') }}" class="btn btn-success col-md-12" type="button">Registrar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table" id="tabla-precios">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Precio al publico</th>
                    <th scope="col">Precio mayoreo</th>
                    <th scope="col">Precio interno</th>
                    <th scope="col">Precio taller</th>
                    <th scope="col">Precio otras distribuidoras</th>
                    <th scope="col">Impuesto</th>
                    <th scope="col">- </th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                    <tr>
                        <td scope="row">{{ $item->id }}</td>
                        <td>{{ $item->precio_publico }}</td>
                        <td>{{ $item->precio_mayoreo }}</td>
                        <td>{{ $item->precio_interno }}</td>
                        <td>{{ $item->precio_taller }}</td>
                        <td>{{ $item->precio_otras_distribuidoras }}</td>
                        <td>{{ $item->impuesto}}</td>
                        <td><a href="{{ base_url('catalogos/preciosController/editar/'.$item->id) }}" class="btn btn-primary" type="button">Editar</a></td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$("#tabla-precios").DataTable({});
</script>
<script type="text/javascript">
        $(document).ready(function(){
            $("#menu_refacciones").addClass("show");
            $("#refacciones_alternos").addClass("show");
            $("#refacciones_alternos").addClass("active");
            $("#forms").addClass("show");
            $("#forms").addClass("active");
            $("#menu_alternos_poliza_actualizacion").addClass("active");
            $("#M02").addClass("active");
        });
</script>
@endsection