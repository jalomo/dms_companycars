<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TallerController extends MX_Controller
{


    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');
        $dataFromApi = $this->curl->curlGet('api/talleres/');
        $dataVendedores = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = isset($dataVendedores) ? $dataVendedores : [];
        $data['titulo'] = "Talleres";
        $this->blade->render('talleres/listado', $data);
    }

    public function crear()
    {
        $this->load->library('curl');
        $this->load->helper('general');
        $data['titulo'] = "Registrar taller";
        $this->blade->render('talleres/formulario', $data);
    }

    public function editar($id)
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');
            $dataFromApi = $this->curl->curlGet('api/talleres/' . $id);
            $datavendedor = procesarResponseApiJsonToArray($dataFromApi);
            $data['data'] = isset($datavendedor)  ? $datavendedor : [];
            $data['titulo'] = "Editar de Taller";
            $this->blade->render('talleres/formulario', $data);
        }
    }
}

/* End of file VendedoresController.php */
