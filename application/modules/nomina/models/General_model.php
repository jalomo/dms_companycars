<?php defined('BASEPATH') or exit('No direct script access allowed');

class General_model  extends CI_Model  {

	function __construct()
    {
        parent::__construct();
    }

    public function form($url,$id = false){

        $parameters = (($id == false)? array() : array('id'=>$id));
        $response = $this->call_api($url,$parameters);
        $compilacion = array();
        if(is_array($response) && array_key_exists('data',$response) && count($response['data'])>0){
            foreach ($response['data'] as $key => $value) {
                $grupo = (array_key_exists('group',$value))? $value['group'] : 'grupo1';
                $compilacion[$grupo][] = $value;
                // if(array_key_exists($grupo,$compilacion)){
                //     $compilacion[$grupo][] = $value;
                // } else {
                //     $compilacion = array($grupo => array($value));
                // }
            }
        }
        
        return $compilacion;

    }

    function call_api($url,$parameters = array(),$type = 'get'){

        if($_SERVER['HTTP_HOST'] == 'localhost'){
            $this->api_path = 'http://localhost/sistema_nomina/';
        }else{
            $this->api_path = 'https://www.sohex.net/nomina_dms/';
        }
        $url = $this->api_path.$url;

        $this->load->library('Restclient');
        switch ($type) {
            case 'post':
                $response = $this->restclient->post($url,$parameters);
                break;
            case 'put':
                $response = $this->restclient->put($url,$parameters);
                break;
            case 'delete':
                $response = $this->restclient->delete($url,$parameters);
                break;
            default:
                $response = $this->restclient->get($url,$parameters);
                break;
        }

        if(is_array($response) && count($response)>0){
            $response['code'] = $this->restclient->http_code();
        }
        return $response;
        
    }

    public function upload_file($archivo){
        
        if($_SERVER['HTTP_HOST'] == 'localhost'){
            $this->api_path = 'http://localhost/sistema_nomina/';
        }else{
            $this->api_path = 'https://www.sohex.net/nomina_dms/';
        }
        $target_url = $this->api_path.'archivos/guardar';

        if (function_exists('curl_file_create')) { // php 5.5+
            $cFile = curl_file_create($archivo['tmp_name'],$archivo['type'],$archivo['name']);
        } else { // 
            $cFile = '@' . realpath($archivo['tmp_name']);
        }
        $post = array('archivo'=> $cFile);
        
        $ch = curl_init($target_url);
        // curl_setopt($ch, CURLOPT_URL,$target_url);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $results=curl_exec($ch);
        curl_close ($ch);
        
        return @json_decode($results);
    }

    
}