<?php defined('BASEPATH') or exit('No direct script access allowed');

class CaTables_model  extends CI_Model  {

    public $db_nomina;
    public $dbf_nomina;

	function __construct()
    {
        parent::__construct();
        $this->dbf_nomina = $this->load->dbforge('nomina', TRUE);
    }

    public function createTemporal(){
        utils::pre($_SESSION);

        $this->dbf_nomina->create_table('table_name', TRUE);
    }
}