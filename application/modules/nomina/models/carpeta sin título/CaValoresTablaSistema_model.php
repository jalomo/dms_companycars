<?php defined('BASEPATH') or exit('No direct script access allowed');

class CaValoresTablaSistema_model  extends CI_Model  {

    public $db_nomina;
	function __construct()
    {
        parent::__construct();
        $this->db_nomina = $this->load->database('nomina', TRUE);
    }

    public function getList($where = array()){
        $this->db_nomina
            ->select('id,id_TablaSistema,Valor_1,Valor_2,Valor_3')
            ->from('ca_valorestablasistema');
        $query = $this->db_nomina->get();
        $listado = ($query->num_rows() > 0)? $query->result_array() : false;
        $query->free_result();
        return $listado;
    }

    public function delete($id){
        $this->db_nomina->where('id_TablaSistema',$id);
        return $this->db_nomina->delete('ca_valorestablasistema')?true: false;
    }

    public function insert($content){
        // $this->load->library('uuid');
        // $id = $this->uuid->v4();
        // $this->db_nomina->set('id',$id);
        $this->db_nomina->set('FechaRegistro',date("Y-m-d H:i:s"));
        return $this->db_nomina->insert('ca_valorestablasistema',$content)? true : false;
    }

    public function generateTemp($id_TablaSistema,$where = array()){

        $idUsuario = $this->session->userdata('id');
        $this->db_nomina->where('id_Usuario',$idUsuario);
        $this->db_nomina->where('id_TablaSistema',$id_TablaSistema);
        $this->db_nomina->delete('ca_valorestablasistemaTemp');

        $this->db_nomina
            ->select('id,id_TablaSistema,Valor_1,Valor_2,Valor_3')
            ->from('ca_valorestablasistema')
            ->where('id_TablaSistema',$id_TablaSistema)
            ->order_by('FechaRegistro','asc');
        $query = $this->db_nomina->get();
        $listado = ($query->num_rows() > 0)? $query->result_array() : false;
        $query->free_result();

        if(is_array($listado)){
            $this->load->library('uuid');
            foreach ($listado as $key => $value) {
                $insert = array(
                    'id' => $this->uuid->v4(),
                    'id_Origen' => $value['id'],
                    'id_Usuario' => $idUsuario,
                    'id_TablaSistema' => $id_TablaSistema,
                    'Valor_1' => $value['Valor_1'],
                    'Valor_2' => $value['Valor_2'],
                    'Valor_3' => $value['Valor_3']
                );
                $this->db_nomina->set('FechaRegistro',date("Y-m-d H:i:s"));
                $this->db_nomina->insert('ca_valorestablasistemaTemp',$insert);
            }
        }

        $this->db_nomina
            ->select('id,id_Usuario,id_TablaSistema,Valor_1,Valor_2,Valor_3')
            ->from('ca_valorestablasistemaTemp')
            ->where('id_TablaSistema',$id_TablaSistema)
            ->where('id_Usuario',$idUsuario);
        return $this->db_nomina->count_all_results();
    }

    public function getListDataTemp($id_TablaSistema,$idUsuario){

        $this->db_nomina
            ->select('id,id_Origen,id_Usuario,id_TablaSistema,Valor_1,Valor_2,Valor_3')
            ->from('ca_valorestablasistemaTemp')
            ->where('id_TablaSistema',$id_TablaSistema)
            ->where('id_Usuario',$idUsuario);
            
        $query = $this->db_nomina->get();
        $listado = ($query->num_rows() > 0)? $query->result_array() : false;

        $query->free_result();
        return $listado;
    }

    public function getRowDataTemp($id){

        $this->db_nomina
            ->select('id,id_Usuario,id_TablaSistema,Valor_1,Valor_2,Valor_3')
            ->from('ca_valorestablasistemaTemp')
            ->where('id',$id);
            
        $query = $this->db_nomina->get();
        $listado = ($query->num_rows() > 0)? $query->row_array() : false;

        $query->free_result();
        return $listado;
    }

    public function setRowDataTemp($content){
        $this->load->library('uuid');
        $id = $this->uuid->v4();
        $this->db_nomina->set('id',$id);
        $this->db_nomina->set('id_Origen',$this->uuid->v4());
        $this->db_nomina->set('id_Usuario',$this->session->userdata('id'));
        $this->db_nomina->set('FechaRegistro',date("Y-m-d H:i:s"));
        $response = $this->db_nomina->insert('ca_valorestablasistemaTemp',$content);
        return $response? $id : false;
    }

    

    public function updateRowDataTemp($id,$content){
        $this->db_nomina->where('id',$id);
        $response = $this->db_nomina->update('ca_valorestablasistemaTemp',$content);
        return $response;
    }
}