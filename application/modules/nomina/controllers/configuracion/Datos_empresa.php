<?php defined('BASEPATH') or exit('No direct script access allowed');

class Datos_empresa extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/configuracion/datos_empresa/');
        $this->breadcrumb = array(
            'Configuración',
            'Parámetros del sistema',
            array('name'=>'Datos de la empresa','url'=>site_url('nomina/configuracion/datos_empresa/'))
        );
    }

    public function obtener_tabs($id){
        $contentForm = array(
            'id' => $id,
            'tab_1' => '',
            'tab_2' => '',
            'tab_3' => '',
            
            'url_1' => site_url('nomina/configuracion/datos_empresa/index?id='.$id),
            'url_2' => site_url('nomina/configuracion/datos_empresa/datos_patron?id='.$id),
            'url_3' => site_url('nomina/configuracion/datos_empresa/domicilio_fiscal?id='.$id),

            'content_form' => ''
        );
        return $contentForm;
    }


    public function form($form){
        $this->load->helper('form');
        if(is_array($form)){
            foreach ($form as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if($value2['key'] != 'id_Trabajador' && $value2['key'] != 'Clave'){
                        if($value2['input']['catalog'] != false){
                            $opciones = array();
                            foreach ($value2['input']['catalog'] as $value3) {
                                if(array_key_exists('Clave',$value3)){
                                    $opciones[$value3['id']] = $value3['Clave'].' - '.$value3['Descripcion'];
                                }else{
                                    $opciones[$value3['id']] = $value3['Descripcion'];
                                }        
                            }
                            $form[$key][$key2]['input'] = form_dropdown( $value2['input']['name'], $opciones, $value2['input']['value'], 'id="'.$value2['input']['id'].'" class="'.$value2['input']['class'].'" attr-id="'.$value2['input']['value'].'"' );
                        }else{
                            $form[$key][$key2]['input'] = form_input($value2['input']);
                        }
                        $form[$key][$key2]['key'] = $value2['input']['id'];
                        $form[$key][$key2]['value'] = $value2['input']['value'];
                    }else{
                        unset($form[$key][$key2]);
                    }
                }
            }
        }
        return $form;
    }

    public function index($empresa = 1){
        $this->breadcrumb[] = array('name'=>'Razon social y logo','url'=>site_url('nomina/configuracion/datos_empresa/index/'.$empresa));
        $this->scripts[] = script_tag($this->pathScript.'index.js');

        $this->load->model('General_model');
        $contenido_form = $this->General_model->form('configuracion/empresa/store_form',array('id'=>$empresa),'get');
        $contenido_form = $this->form($contenido_form);

        $obtener_tabs = $this->obtener_tabs($empresa);
        //$obtener_tabs['detalle_form'] = $detalle;
        $obtener_tabs['tab_1'] = 'active';

        $this->load->library('parser');
        $obtener_tabs['content_form'] = $this->parser->parse('/configuracion/datos_empresa/index',$contenido_form,true);
        

        $html = $this->parser->parse('/configuracion/datos_empresa/tabs',$obtener_tabs,true);
        $this->output($html);
    }

    public function datos_patron($empresa = 1){
        $this->scripts[] = script_tag($this->pathScript.'datos_patron.js');
        $this->breadcrumb[] = array('name'=>'Registros patronales','url'=>site_url('nomina/configuracion/datos_empresa/datos_patron/'.$empresa));

        
        $obtener_tabs = $this->obtener_tabs($empresa);
        //$obtener_tabs['detalle_form'] = $detalle;
        $obtener_tabs['tab_2'] = 'active';

        $this->load->library('parser');
        $obtener_tabs['content_form'] = $this->parser->parse('/configuracion/datos_empresa/datos_patron',array(),true);
        

        $html = $this->parser->parse('/configuracion/datos_empresa/tabs',$obtener_tabs,true);
        $this->output($html);
    }


    public function datos_patron_get()
    {
        $identity = $this->input->get_post('id');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('configuracion/patron/store_findAll',array('id_Empresa' => $identity),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    }


    public function datos_patron_alta($empresa = 1){

        $this->breadcrumb[] = array('name'=>'Registros patronales','url'=>site_url('nomina/configuracion/datos_empresa/datos_patron/'.$empresa));
        $this->breadcrumb[] = array('name'=>'Alta de patron','url'=>site_url('nomina/configuracion/datos_empresa/datos_patron_alta/'.$empresa));
        
        $this->scripts[] = script_tag($this->pathScript.'datos_patron_alta.js');

        $this->load->model('General_model');
        $contenido_form = $this->General_model->form('configuracion/patron/store_form',array(),'get');
        $contenido_form = $this->form($contenido_form);

        $parametros = $this->General_model->call_api('nomina/trabajador/parametrosnominagen/store_find',array('id'=>1),'get');
        $contenido_form['salario_minimo'] = $parametros['data']['SalarioMinimoSM'];

        $clave = $this->General_model->call_api('configuracion/patron/store_clave',array(),'get');
        
        $contenido_form['clave'] = $clave['data']['Clave'];
        $contenido_form['id_Empresa'] = $empresa;

        

        $obtener_tabs = $this->obtener_tabs($empresa);
        $obtener_tabs['tab_2'] = 'active';

        $this->load->library('parser');
        $obtener_tabs['content_form'] = $this->parser->parse('/configuracion/datos_empresa/datos_patron_form',$contenido_form,true);
        

        $html = $this->parser->parse('/configuracion/datos_empresa/tabs',$obtener_tabs,true);
        $this->output($html);
    }

    public function datos_patron_editar($empresa = 1,$id_patron){

        $this->breadcrumb[] = array('name'=>'Registros patronales','url'=>site_url('nomina/configuracion/datos_empresa/datos_patron/'.$empresa));
        $this->breadcrumb[] = array('name'=>'Alta de patron','url'=>site_url('nomina/configuracion/datos_empresa/datos_patron_alta/'.$empresa));
        
        $this->scripts[] = script_tag($this->pathScript.'datos_patron_editar.js');

        $this->load->model('General_model');
        $contenido_form = $this->General_model->form('configuracion/patron/store_form',array('id'=>$id_patron),'get');
        $contenido_form = $this->form($contenido_form);

        $parametros = $this->General_model->call_api('configuracion/patron/store_find',array('id'=>$id_patron),'get');
        
        $contenido_form['salario_minimo'] = $parametros['data']['SalarioMinimo'];
        $contenido_form['clave'] = $parametros['data']['Clave'];
        $contenido_form['id_Empresa'] = $empresa;
        $contenido_form['id_Patron'] = $id_patron;

        

        $obtener_tabs = $this->obtener_tabs($empresa);
        $obtener_tabs['tab_2'] = 'active';

        $this->load->library('parser');
        $obtener_tabs['content_form'] = $this->parser->parse('/configuracion/datos_empresa/datos_patron_form',$contenido_form,true);
        

        $html = $this->parser->parse('/configuracion/datos_empresa/tabs',$obtener_tabs,true);
        $this->output($html);
    }

    public function domicilio_fiscal($empresa = 1){
        $this->breadcrumb[] = array('name'=>'Domicilio fiscal','url'=>site_url('nomina/configuracion/datos_empresa/domicilio_fiscal/'.$empresa));
        $this->scripts[] = script_tag($this->pathScript.'domicilio_fiscal.js');

        $this->load->model('General_model');
        $contenido_form = $this->General_model->form('configuracion/domicilio_fiscal/store_form',array('id'=>$empresa),'get');
        $contenido_form = $this->form($contenido_form);
        $contenido_form['id_Empresa'] = $empresa;

        $obtener_tabs = $this->obtener_tabs($empresa);
        //$obtener_tabs['detalle_form'] = $detalle;
        $obtener_tabs['tab_3'] = 'active';

        $this->load->library('parser');
        $obtener_tabs['content_form'] = $this->parser->parse('/configuracion/datos_empresa/domicilio_fiscal',$contenido_form,true);
        

        $html = $this->parser->parse('/configuracion/datos_empresa/tabs',$obtener_tabs,true);
        $this->output($html);
    }


    // public function alta($id_trabajador){
    //     $this->scripts[] = script_tag($this->pathScript.'alta.js');
    //     $this->breadcrumb[] = 'Alta';

    //     $this->load->model('General_model');
    //     $trabajador = $this->General_model->call_api('nomina/trabajador/datosgenerales/store_find',array('id'=>$id_trabajador),'get');
    //     $falta = $this->General_model->form('catalogos/faltas/store_form',array(),'get');
    //     $dataForm = $this->form($falta);
    //     $dataForm['id_trabajador'] = $id_trabajador;
    //     $dataForm['trabajador'] = $trabajador['data'];

    //     $this->load->library('parser');
    //     $html = $this->parser->parse('/inicio/faltas/alta', $dataForm,true);
        
    //     $this->output($html);
    // }

    // public function alta_guardar(){

    //     $parametros = $this->input->post();
    //     $id_Trabajador = $this->input->post('id_Trabajador');
    //     $Fecha = $this->input->post('Fecha');
        
    //     $this->load->model('General_model');
    //     $datos = $this->General_model->call_api('catalogos/faltas/store_find',array('id_Trabajador'=>$id_Trabajador,'Fecha'=>$Fecha),'get');

    //     if($datos['data'] === false){
    //         $response = $this->General_model->call_api('catalogos/faltas/store',$parametros,'post');
    //         $code = (array_key_exists('code',$response))? $response['code'] : 200;
    //     }else{
    //         $code = 406;
    //         $response['status'] = 'error';
    //         $response['message']['Fecha'] = 'Ya se encuentra registrado';
    //     }
    //     $this->response($response,$code);
    // }


    // public function editar($id,$id_trabajador){
        
    //     $this->scripts[] = script_tag($this->pathScript.'editar.js');
    //     $this->breadcrumb[] = 'Editar';

    //     $this->load->model('General_model');
    //     $trabajador = $this->General_model->call_api('nomina/trabajador/datosgenerales/store_find',array('id'=>$id_trabajador),'get');
    //     $falta = $this->General_model->form('catalogos/faltas/store_form',array('id'=>$id),'get');
    //     $dataForm = $this->form($falta);
    //     $dataForm['id_trabajador'] = $id_trabajador;
    //     $dataForm['id'] = $id;
    //     $dataForm['trabajador'] = $trabajador['data'];

    //     $this->load->library('parser');
    //     $html = $this->parser->parse('/inicio/faltas/editar', $dataForm,true);
        
    //     $this->output($html);
    // }

    public function editar_guardar(){

        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('configuracion/empresa/store',$parametros,'put');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        $this->response($response,$code);
    }

    public function datos_patron_alta_guardar(){

        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('configuracion/patron/store',$parametros,'post');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        $this->response($response,$code);
    }
    public function datos_patron_alta_actualizar(){

        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('configuracion/patron/store',$parametros,'put');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        $this->response($response,$code);
    }
    public function datos_patron_delete(){

        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('configuracion/patron/store',$parametros,'delete');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        $this->response($response,$code);
    }

    public function domicilio_fiscal_actualizar(){

        $parametros = $this->input->post();
        $id_Empresa = $this->input->post('id_Empresa');
        
        $this->load->model('General_model');
        $datos = $this->General_model->call_api('configuracion/domicilio_fiscal/store_find',array('id_Empresa'=>$id_Empresa),'get');
        if($datos['data'] == false){
            $response = $this->General_model->call_api('configuracion/domicilio_fiscal/store',$parametros,'post');
        }else{
            $parametros['id'] = $datos['data']['id'];
            $response = $this->General_model->call_api('configuracion/domicilio_fiscal/store',$parametros,'put');
        }

        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        $this->response($response,$code);
    }

    

    // public function delete(){

    //     $parametros = $this->input->post();
        
    //     $this->load->model('General_model');
    //     $response = $this->General_model->call_api('catalogos/faltas/store',$parametros,'delete');
    //     $code = (array_key_exists('code',$response))? $response['code'] : 200;
        
    //     $this->response($response,$code);
    // }
}