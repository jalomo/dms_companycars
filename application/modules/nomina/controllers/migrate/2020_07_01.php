<?php defined('BASEPATH') or exit('No direct script access allowed');

$this->forge->drop_foreign_key('ca_puestos','id_Departamento');
$this->forge->drop_foreign_key('ca_puestos','id_RiesgoPuesto');
$this->forge->drop_foreign_key('ca_trabajadores','id_Departamento');
$this->forge->drop_foreign_key('ca_trabajadores','id_EntidadNacimiento');
$this->forge->drop_foreign_key('ca_trabajadores','id_Puesto');
$this->forge->drop_foreign_key('ca_trabajadores','id_Clasificacion','ca_clasificaciones(id)');
$this->forge->drop_foreign_key('ca_valorestablasistema','id_TablaSistema');

# ca_departamentos
$this->forge->drop_table('ca_departamentos',TRUE);
$this->forge->add_field(array(
    'id' => array( 'type' => 'CHAR', 'constraint' => 36, 'unique' => TRUE ),
    'Clave' => array( 'type' => 'INT', 'constraint' => 11 ),
    'Descripcion' => array( 'type' => 'VARCHAR', 'constraint' => 250 ),
    'CuentaCOI' => array( 'type' => 'VARCHAR','constraint' => 250 ),
    'DepartamentoCOI' => array( 'type' => 'VARCHAR','constraint' => 250 ),
    'FechaRegistro' => array( 'type' => 'timestamp','constraint' => 0 ),
));
$this->forge->add_key('id', TRUE);
$this->forge->create_table('ca_departamentos',true);

# ca_puestos
$this->forge->drop_table('ca_puestos',TRUE);
$this->forge->add_field(array(
    'id' => array( 'type' => 'CHAR', 'constraint' => 36, 'unique' => TRUE ),
    'Clave' => array( 'type' => 'INT', 'constraint' => 11 ),
    'Descripcion' => array( 'type' => 'VARCHAR', 'constraint' => 250 ),
    'SalarioDiario' => array( 'type' => 'decimal','constraint' => '11,2' ),
    'SalarioMaximo' => array( 'type' => 'decimal','constraint' => '11,2' ),
    'id_RiesgoPuesto' => array( 'type' => 'INT','constraint' => 11 ),
    'FechaRegistro' => array( 'type' => 'timestamp','constraint' => 0 ),
));
$this->forge->add_key('id', TRUE);
$this->forge->create_table('ca_puestos',true);

# ca_riesgopuesto
$this->forge->drop_table('ca_riesgopuesto',TRUE);
$this->forge->add_field(array(
    'id' => array( 'type' => 'INT', 'constraint' => 11, 'unique' => TRUE,'auto_increment' => TRUE ),
    'Numero' => array( 'type' => 'INT', 'constraint' => 11 ),
    'Descripcion' => array( 'type' => 'VARCHAR', 'constraint' => 250 ),
    'FechaRegistro' => array( 'type' => 'timestamp','constraint' => 0 ),
));
$this->forge->add_key('id', TRUE);
$this->forge->create_table('ca_riesgopuesto',true);
$data = array(
    array('id' => 1,'Numero' => 1, 'Descripcion' => 'Clase I','FechaRegistro'=>date("Y-m-d H:i:s")),
    array('id' => 2,'Numero' => 2, 'Descripcion' => 'Clase II','FechaRegistro'=>date("Y-m-d H:i:s")),
    array('id' => 3,'Numero' => 3, 'Descripcion' => 'Clase III','FechaRegistro'=>date("Y-m-d H:i:s")),
    array('id' => 4,'Numero' => 4, 'Descripcion' => 'Clase IV','FechaRegistro'=>date("Y-m-d H:i:s")),
    array('id' => 5,'Numero' => 5, 'Descripcion' => 'Clase V','FechaRegistro'=>date("Y-m-d H:i:s"))
);
$this->db_nomina->insert_batch('ca_riesgopuesto', $data);

#ca_puestos
$this->forge->add_foreign_key('ca_puestos','id_RiesgoPuesto','ca_riesgopuesto(id)');

# ca_clasificaciones
$this->forge->drop_table('ca_clasificaciones',TRUE);
$this->forge->add_field(array(
    'id' => array( 'type' => 'CHAR', 'constraint' => 36, 'unique' => TRUE ),
    'Clave' => array( 'type' => 'INT', 'constraint' => 11 ),
    'Descripcion' => array( 'type' => 'VARCHAR', 'constraint' => 250 ),
    'FechaRegistro' => array( 'type' => 'timestamp','constraint' => 0 ),
));
$this->forge->add_key('id', TRUE);
$this->forge->create_table('ca_clasificaciones',true);

#ca_valorestablasistema
$this->forge->drop_table('ca_valorestablasistema',TRUE);
$this->forge->add_field(array(
    'id' => array( 'type' => 'CHAR', 'constraint' => 36, 'unique' => TRUE ),
    'id_TablaSistema' => array( 'type' => 'CHAR', 'constraint' => 36),
    'Valor_1' => array( 'type' => 'VARCHAR', 'constraint' => 250 ),
    'Valor_2' => array( 'type' => 'VARCHAR', 'constraint' => 250 ),
    'Valor_3' => array( 'type' => 'VARCHAR', 'constraint' => 250 ),
    'FechaRegistro' => array( 'type' => 'timestamp','constraint' => 0 )
));
$this->forge->add_key('id', TRUE);
$this->forge->create_table('ca_valorestablasistema',true);


#ca_valorestablasistemaTemp
$this->forge->drop_table('ca_valorestablasistemaTemp',TRUE);
$this->forge->add_field(array(
    'id' => array( 'type' => 'CHAR', 'constraint' => 36, 'unique' => TRUE ),
    'id_Origen' => array( 'type' => 'CHAR', 'constraint' => 36),
    'id_TablaSistema' => array( 'type' => 'CHAR', 'constraint' => 36),
    'id_Usuario' => array( 'type' => 'INT', 'constraint' => 11),
    'Valor_1' => array( 'type' => 'VARCHAR', 'constraint' => 250 ),
    'Valor_2' => array( 'type' => 'VARCHAR', 'constraint' => 250 ),
    'Valor_3' => array( 'type' => 'VARCHAR', 'constraint' => 250 ),
    'FechaRegistro' => array( 'type' => 'timestamp','constraint' => 0 )
));
$this->forge->add_key('id', TRUE);
$this->forge->create_table('ca_valorestablasistemaTemp',true);


# ca_tablassistema
$this->forge->drop_table('ca_tablassistema',TRUE);
$this->forge->add_field(array(
    'id' => array( 'type' => 'CHAR', 'constraint' => 36, 'unique' => TRUE ),
    'Clave' => array( 'type' => 'INT', 'constraint' => 11 ),
    'id_TipoCatalogo' => array( 'type' => 'INT', 'constraint' => 11 ),
    'Descripcion' => array( 'type' => 'VARCHAR', 'constraint' => 250 ),
    'FechaRegistro' => array( 'type' => 'timestamp','constraint' => 0 ),
));
$this->forge->add_key('id', TRUE);
$this->forge->create_table('ca_tablassistema',true);
$data = array(
    array('id' => utils::guid(),'Clave' => 1, 'id_TipoCatalogo'=>2, 'Descripcion' => 'ISR Mes 2018','FechaRegistro'=>date("Y-m-d H:i:s")),
    array('id' => utils::guid(),'Clave' => 2, 'id_TipoCatalogo'=>2, 'Descripcion' => 'ISR Año 2018','FechaRegistro'=>date("Y-m-d H:i:s")),
    array('id' => utils::guid(),'Clave' => 3, 'id_TipoCatalogo'=>2, 'Descripcion' => 'Subsidio Empleo','FechaRegistro'=>date("Y-m-d H:i:s")),
    array('id' => utils::guid(),'Clave' => 4, 'id_TipoCatalogo'=>2, 'Descripcion' => 'Salario Diario','FechaRegistro'=>date("Y-m-d H:i:s")),
    array('id' => utils::guid(),'Clave' => 5, 'id_TipoCatalogo'=>2, 'Descripcion' => 'Vacaciones','FechaRegistro'=>date("Y-m-d H:i:s")),
    array('id' => utils::guid(),'Clave' => 6, 'id_TipoCatalogo'=>2, 'Descripcion' => 'Vac. S.R. 1 día','FechaRegistro'=>date("Y-m-d H:i:s")),
    array('id' => utils::guid(),'Clave' => 7, 'id_TipoCatalogo'=>2, 'Descripcion' => 'Vac. S.R. 2 día','FechaRegistro'=>date("Y-m-d H:i:s")),
    array('id' => utils::guid(),'Clave' => 8, 'id_TipoCatalogo'=>2, 'Descripcion' => 'Vac. S.R. 3 día','FechaRegistro'=>date("Y-m-d H:i:s")),
    array('id' => utils::guid(),'Clave' => 9, 'id_TipoCatalogo'=>2, 'Descripcion' => 'Vac. S.R. 4 día','FechaRegistro'=>date("Y-m-d H:i:s")),
    array('id' => utils::guid(),'Clave' => 10, 'id_TipoCatalogo'=>2, 'Descripcion' => 'Vac. S.R. 5 día','FechaRegistro'=>date("Y-m-d H:i:s")),
    array('id' => utils::guid(),'Clave' => 11, 'id_TipoCatalogo'=>2, 'Descripcion' => 'Faltas Sem.Red','FechaRegistro'=>date("Y-m-d H:i:s")),
);
$this->db_nomina->insert_batch('ca_tablassistema', $data);

#ca_valorestablasistema
$this->forge->add_foreign_key('ca_valorestablasistema','id_TablaSistema','ca_tablassistema(id)');