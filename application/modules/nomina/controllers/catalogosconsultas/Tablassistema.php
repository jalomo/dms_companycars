<?php defined('BASEPATH') or exit('No direct script access allowed');

class Tablassistema extends CI_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = 'js/nomina/catalogosconsultas/tablassistema/';
        $this->pathBase = '/nomina/catalogosconsultas/tablassistema';

        $this->breadcrumb = array(
            'Catalogos y consultas',
            array('name'=>'Catálogos del Sistema','url'=>site_url('nomina/catalogosconsultas/catalogossistema')),
            array('name'=>'Tablas del sistema','url'=>site_url('nomina/catalogosconsultas/tablassistema'))
        );

    }

    public function index()
    {
        $this->title = 'Tablas del sistema';
        $this->breadcrumb[] = 'Listado';
        $this->scripts[] = utils::script_tag($this->pathScript.'index.js');

        $html = $this->load->view('/catalogosconsultas/tablassistema/index',false,true);
        $this->output($html);
    }

    public function nuevo(){
        $this->title = 'Alta de tabla';
        $this->breadcrumb[] = 'Alta de tabla';
        $this->scripts[] = utils::script_tag($this->pathScript.'nuevo.js');

        $this->load->library('uuid');
        $uuid_temp = $this->uuid->v4();
        
        $dataForm = array(
            'uuid_temp' => $uuid_temp,
            'id_Usuario' => $this->session->userdata('id'),
        );
        
        $this->load->library('parser');
        $html = $this->parser->parse('/catalogosconsultas/tablassistema/form',$dataForm,true);
        $this->output($html);
    }

    public function editar()
    {
        $this->title = 'Cambios de tablas';
        $this->breadcrumb[] = 'Cambios de tablas';
        $this->scripts[] = utils::script_tag($this->pathScript.'editar.js');
        
        $this->load->library('uuid');
        $uuid_temp = $this->uuid->v4();
        $id = $this->input->get('id');

        $dataForm = array(
            'id' => $id,
            'uuid_temp' => $uuid_temp
        );
        
        $this->load->library('parser');
        $html = $this->parser->parse('/catalogosconsultas/tablassistema/form',$dataForm,true);

        $this->output($html);
    }

    public function output($html){
		$this->blade->render('build', array(
            'scripts' => $this->scripts,
            'titulo_modulo' => $this->title,
            'modulo' => 'Nomina',
            'breadcrumb' => $this->breadcrumb,
            'content'=>$html
        ));
    }

}
