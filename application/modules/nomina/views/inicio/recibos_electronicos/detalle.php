<style type="text/css">
.myTable { background-color:#fff;border-collapse:collapse; }
.myTable th { background-color:silver;color:#000;width:50%; }
.myTable td, .myTable th { padding:5px;border:1px solid silver; }

.myTable2 { background-color:#fff;border-collapse:collapse; }
.myTable2 th { background-color:silver;color:#000;width:50%; }
.myTable2 td, .myTable2 th { padding:5px;border:1px solid #fff; }
</style>

<div>
	<table style="width:100%" border="0" cellpadding="2" cellspacing="2">
		<tr>
			<td>
				<h2><?php echo $empresa['RazonSocial']; ?></h2>
				<h5>CFDI de Nómina</h5>
			</td>
			<td style="width:300px;" >
				<table style="width:100%" class="myTable" border="1" cellpadding="2" cellspacing="2">
					<tr >
						<th>
							Tipo de nómina
						</td>
						<th>
							Fecha
						</td>
					</tr>
					<tr>
						<td>
							Recibo de nómina
						</td>
						<td>
							<?php echo utils::aFecha(date('Y-m-d'),true); ?>
						</td>
					</tr>
				</table>

			</td>
		</tr>
	</table>
	<br/>
	<table style="width:100%" border="0" cellpadding="2" cellspacing="2">
		<tr>
			<td>
			<table style="" class="myTable" border="1" cellpadding="2" cellspacing="2">
					<tr >
						<th>
							Empleado
						</th>
					</tr>
					<tr>
						<td style="height:230px;" >

						<table class="myTable2" style="width:100%;height:230px;" border="0" >
							<tr >
								<td style="text-align: right;" >Nombre:</td>
								<td colspan="1">
									<?php echo $trabajador['Clave'].'&nbsp;&nbsp;'.$trabajador['Apellido_1'].' '.$trabajador['Apellido_2'].' '.$trabajador['Nombre']; ?>
								</td>
							</tr>
							<tr>
								<td style="text-align: right;">Depto.:</td>
								<td colspan="1">
									<?php echo $trabajador['Descripcion_Departamento']; ?>
								</td>
							</tr>
							<tr>
								<td style="text-align: right;">Puesto:</td>
								<td colspan="1">
									<?php echo $trabajador['Descripcion_Puesto']; ?>
								</td>
							</tr>

							<tr>
								<td style="text-align: right;">RFC:</td>
								<td><?php echo $trabajador['RFC']; ?></td>
								
							</tr>
							<tr>
								<td style="text-align: right;">CURP:</td>
								<td><?php echo $trabajador['NumeroImss']; ?></td>
								
							</tr>
						
						</table>


						</td>
					</tr>
					
				</table>

			</td>
			<td>
				<table style="width:100%" class="myTable" border="1" cellpadding="2" cellspacing="2">
					<tr >
						<th>
							Seguridad Social
						</th>
					</tr>
					<tr>
						<td style="height:230px;" >

						<table class="myTable2" style="width:100%;" border="0" >
						<tr>
							<td style="text-align: right;">NSS:</td>
							<td><?php echo $trabajador['NumeroImss']; ?></td>
						</tr>
						<tr>
							<td style="text-align: right;">Salario diario:</td>
							<td>$ <?php echo utils::formatMoney($salario['SalarioDia']); ?></td>
						<tr>
						<tr>
							<td style="text-align: right;">Salario integrado:</td>
							<td>$ <?php echo utils::formatMoney($salario['SDICapturado']); ?></td>
						<tr>
						<tr>
							<td style="text-align: right;">Fecha de ingreso:</td>
							<td><?php echo utils::aFecha($trabajador['FechaAlta'],false); ?></td>
						<tr>
						<tr>
							<td style="text-align: right;">Dias pagados:</td>
							<td><?php echo $salario['DiasPeriodo']; ?></td>
						<tr>
						<tr>
							<td style="text-align: right;">Periodo:</td>
							<td>
								Del <?php echo utils::aFecha($periodo['FechaInicio'],true); ?><br/> al <?php echo utils::aFecha($periodo['FechaFin'],true); ?>
							</td>
						<tr>
					</table>


						</td>
					</tr>
					
				</table>

			</td>
		</tr>
		
	</table>
	<br/>

	<?php 
	$percepciones_count = is_array($percepciones) && count($percepciones)>0? count($percepciones) : 0;
	$deducciones_count = is_array($deducciones) && count($deducciones)>0? count($deducciones) : 0;
	$cantidad = 0;
	if($percepciones_count > $deducciones_count){
		$cantidad = $percepciones_count;
	}else{
		$cantidad = $deducciones_count;
	}
	?>
	<table class="myTable2" border="0" cellpadding="3" cellspacing="3">
		<tr>
			<th colspan="3">Percepciones</th>

			<th colspan="3">Deducciones</th>
		</tr>
	<?php for ($i=0; $i < $cantidad   ; $i++) { ?>
		<?php $p_Clave = (array_key_exists($i,$percepciones))? $percepciones[$i]['Clave'] : ''; ?>
		<?php $p_Descripcion = (array_key_exists($i,$percepciones))? $percepciones[$i]['Descripcion'] : ''; ?>
		<?php $p_Monto = (array_key_exists($i,$percepciones))? '$'.utils::formatMoney($percepciones[$i]['Monto']) : ''; ?>
		<?php $d_Clave = (array_key_exists($i,$deducciones))? $deducciones[$i]['Clave'] : ''; ?>
		<?php $d_Descripcion = (array_key_exists($i,$deducciones))? $deducciones[$i]['Descripcion'] : ''; ?>
		<?php $d_Monto = (array_key_exists($i,$deducciones))? '$'.utils::formatMoney($deducciones[$i]['Monto']) : ''; ?>

		<tr >
			<td style="width: 75px;"><?php echo $p_Clave; ?></td>
			<td><?php echo $p_Descripcion; ?></td>
			<td style="width: 120px;"><?php echo $p_Monto; ?></td>
			
			<td style="width: 75px;"><?php echo $d_Clave; ?></td>
			<td><?php echo $d_Descripcion; ?></td>
			<td style="width: 120px;"><?php echo $d_Monto; ?></td>
		</tr>
	<?php } ?>
	<tr>
		<td style="text-align: right;" colspan="2">Total de percepciones:</td>
		<td >$<?php echo utils::formatMoney($pago['TotalPercepcion']); ?></td>
		<td style="text-align: right;" colspan="2">Total de deducciones:</td>
		<td >$<?php echo utils::formatMoney($pago['TotalDeduccion']); ?></td>
	</tr>
	<tr>
		<td style="text-align: right;" colspan="5">Neto recibido:</td>
		<td >$<?php echo utils::formatMoney($pago['Total']); ?></td>
	</tr>
		
	</table>


<table style="width:100%;" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3" style="background-color:#fff;text-align: left;">
			<br /><br />
			<table style="width:100%;" border="0" cellpadding="10" cellspacing="10">
				<tr >
					<td colspan="2" style="width: 400px;font-size: 11px;text-align:center;">
						*****<?php echo NumeroALetras::convertir($pago['Total'],'PESOS', 'CENTAVOS'); ?>*****
					</td>
				</tr>
				<tr >
					<td style="background-color:#fff;vertical-align: top;text-justify: inter-word;font-size: 10px;">
							La cantidad descrita en este recibo y con ella doy por pagadas todas y cada una de
							las prestaciones a las que legalmente tengo derecho tales como
							salarios ordinarios, horas extras, séptimos días, días de descanso obligatorios,
							etc. Misma que recibo a mi entera satisfacción manifestando mi conformidad
							con los descuentos de la ley que se describen. Por lo que acepto que no se me adeuda
							cantidad alguna por ningún concepto por los servicios prestados
					</td>
					<td style="width:300px;height: 200px;">
						<table style="width:300px;height: 300px;" border="0" cellpadding="2" cellspacing="2">
							<tr >
								<td>

									&nbsp;

								</td>
							</tr>
							<tr >
								<td>
									<hr />
								</td>
							</tr>
							<tr style="background-color:#fff;text-align:center;">
								<td>
									<center>Firma</center>
								</td>
							</tr>
						</table>
					</td>
					<!-- </tr>
			<tr > -->

				</tr>
			</table>
		</td>
	</tr>
</table>
</div>