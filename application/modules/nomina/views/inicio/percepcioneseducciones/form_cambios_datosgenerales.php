<script>
	var identity = "{identity}";
</script>

<div class="row">

	<div class="col-sm-12">
		<div class="mt-1">
			<div class="row">
				<div class="col-sm-12 mb-2">
					<h3>Datos Generales</h3>
				</div>
			</div>
			<div class="card">
				<div class="card-body">
					<h4 class="card-title text-dark">Datos generales de la Percepción o Deducción</h4>


					<form id="general_form">

						<div class="row">
							<div class="col-sm-6">
								{grupo1}
								<div class="form-group row">
									<label for="{key}" class="col-sm-3 col-form-label">{label}</label>
									<div class="col-sm-9">
                                        {input}
									</div>

								</div>
								{/grupo1}
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								{grupo2}
								<div class="form-group row">
									<label for="{key}" class="col-sm-3 col-form-label">{label}</label>
									<div class="col-sm-9">
										{input}
										<small id="msg_{key}" class="form-text text-danger"></small>
									</div>
								</div>
								{/grupo2}
							</div>

							<div class="col-sm-6">
								{grupo4}
								<div class="form-group row">
									<label for="{key}" class="col-sm-3 col-form-label">{label}</label>
									<div class="col-sm-9">
										{input}
										<small id="msg_{key}" class="form-text text-danger"></small>
									</div>
								</div>
								{/grupo4}
							</div>
						</div>


						<div class="row">
							<div class="col-sm-6">
								{grupo3}
								<div class="form-group row">
									<label for="{key}" class="col-sm-3 col-form-label">{label}</label>
									<div class="col-sm-9">
										{input}
										<small id="msg_{key}" class="form-text text-danger"></small>
									</div>
								</div>
								{/grupo3}
							</div>

							<div class="col-sm-6">
								{grupo5}
								<div class="form-group row">
									<label for="{key}" class="col-sm-3 col-form-label">{label}</label>
									<div class="col-sm-9">
										{input}
										<small id="msg_{key}" class="form-text text-danger"></small>
									</div>
								</div>
								{/grupo5}
							</div>
						</div>

						<hr class="style-six" />

						<div class="row">
							<div class="col-sm-9">
								<div class="row">
									<div class="col-sm-4">

										<div class="card">
											<div class="card-body">
												<div class="form-group">
													{grupo6}
													<div class="form-group">
														<label for="{key}">{label}</label>
														{input}
														<small id="msg_{key}" class="form-text text-danger"></small>
													</div>
													{/grupo6}
												</div>
											</div>
										</div>
									</div>

									<div class="col-sm-4">
										<div class="card">
											<div class="card-body">
												<div class="form-group">
													{grupo7}
													<div class="form-group">
														<label for="{key}">{label}</label>
														{input}
														<small id="msg_{key}" class="form-text text-danger"></small>
													</div>
													{/grupo7}
												</div>
											</div>
										</div>
									</div>

									<div class="col-sm-4">
										<div class="card">
											<div class="card-body">
												<div class="form-group">
													{grupo8}
													<div class="form-group">
														<label for="{key}">{label}</label>
														{input}
														<small id="msg_{key}" class="form-text text-danger"></small>
													</div>
													{/grupo8}
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-3">

								{grupo17}
								<div class="form-group">
									<label for="{key}">{label}</label>
									{input}
									<small id="msg_{key}" class="form-text text-danger"></small>
								</div>
								{/grupo17}

							</div>
						</div>



						<div class="row mt-4">
							<div class="col-sm-12">
								<div class="row">
									<div class="col-sm-8">

										<div class="card">
											<div class="card-body">
												<h4 class="card-title text-dark">Interfaz</h4>

												<div class="row">
													<div class="col-sm-6">
														<div class="form-group">
															{grupo10}
															<div class="form-group">
																<label for="{key}">{label}</label>
																{input}
																<small id="msg_{key}"
																	class="form-text text-danger"></small>
															</div>
															{/grupo10}
														</div>
													</div>
													<div class="col-sm-6">
														<div class="form-group">
															{grupo11}
															<div class="form-group">
																<label for="{key}">{label}</label>
																{input}
																<small id="msg_{key}"
																	class="form-text text-danger"></small>
															</div>
															{/grupo11}
														</div>
													</div>
												</div>


											</div>
										</div>
									</div>

									<div class="col-sm-4">
										<div class="card">
											<div class="card-body">
												<div class="form-group">
													{grupo12}
													<div class="form-group">
														<label for="{key}">{label}</label>
														{input}
														<small id="msg_{key}" class="form-text text-danger"></small>
													</div>
													{/grupo12}
												</div>
											</div>
										</div>
									</div>


								</div>
							</div>
						</div>

						<div class="row mt-4">
							<div class="col-sm-12">
								<div class="row">
									<div class="col-sm-12">

										<div class="card">
											<div class="card-body">
												<h4 class="card-title text-dark">Acumulados</h4>

												<div class="row">
													<div class="col-sm-4">
														<div class="form-group">
															{grupo13}
															<div class="form-group">
																<label for="{key}">{label}</label>
																{input}
																<small id="msg_{key}"
																	class="form-text text-danger"></small>
															</div>
															{/grupo13}
														</div>
													</div>
													<div class="col-sm-4">
														<div class="form-group">
															{grupo14}
															<div class="form-group">
																<label for="{key}">{label}</label>
																{input}
																<small id="msg_{key}"
																	class="form-text text-danger"></small>
															</div>
															{/grupo14}
														</div>
													</div>
													<div class="col-sm-4">
														<div class="form-group">
															{grupo15}
															<div class="form-group">
																<label for="{key}">{label}</label>
																{input}
																<small id="msg_{key}"
																	class="form-text text-danger"></small>
															</div>
															{/grupo15}
														</div>
													</div>
												</div>


											</div>
										</div>
									</div>




								</div>
							</div>

						</div>

						<div class="row mt-4">
							<div class="col-sm-12">
								<div class="row">
									<div class="col-sm-12">

										<div class="card">
											<div class="card-body">
												<h4 class="card-title text-dark">Recibos electrónicos</h4>

												<div class="row">
													<div class="col-sm-12">
														<div class="form-group">
															{grupo16}
															<div class="form-group">
																<label for="{key}">{label}</label>
																{input}
																<small id="msg_{key}"
																	class="form-text text-danger"></small>
															</div>
															{/grupo16}
														</div>
													</div>
												</div>


											</div>
										</div>
									</div>




								</div>
							</div>

						</div>


					</form>

				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-12">
		<div class="form-group row mt-3">
			<div class="col-sm-3">&nbsp;</div>
			<div class="col-sm-9">
				<button onclick="Apps.regresar(this);" type="button" class="btn btn-danger col-md-4">Cancelar</button>
				<button onclick="Apps.guardar(this);" type="button" class="btn btn-success col-md-4">Guardar</button>
			</div>
		</div>
	</div>
</div>
