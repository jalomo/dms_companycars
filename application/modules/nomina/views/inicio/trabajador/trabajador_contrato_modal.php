<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
	aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body">
                <form id="contrato_trabajador">
                    <h3 class="mb-3">Contrato</h3>
                    {grupo1}
                        <div id="form_{key}_content" class="form-group">
                            <label for="exampleInputEmail1">{label}</label>
                            {input}
                            <small id="msg_{key}" class="form-text text-danger"></small>
                        </div>
                    {/grupo1}

                    <div id="form_fvencimiento_content" class="form-group">
                        <label for="FechaVencimiento">Fecha de vencimiento</label>
                        <input type="" style="display:block;" class="form-control-plaintext" id="FechaVencimiento" >
                        <input type="date" style="display:none;" class="form-control" name="FechaVencimiento">
                    </div>

                    
                </form>

                <div class="form-group row mt-3">
                    <div class="col-sm-3">&nbsp;</div>
                    <div class="col-sm-9">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button onclick="Apps.guardar_step(this);" type="button" class="btn btn-success col-md-4">Guardar</button>
                    </div>
                </div>

			</div>
		</div>
	</div>
</div>
