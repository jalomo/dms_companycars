<?php if(isset($periodos_trabajador)){ ?>
<?php utils::pre($periodos_trabajador,false); ?>
<?php } ?>

<?php if(isset($montos)){ ?>
	<div class="row mt-3">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-body">
					<!-- <h4 class="mb-4">Totales</h4> -->
					<div class="row">
						<div class="col-sm-3">
							<h4><small>Estatus:</small><br/><?php echo ($montos['Descripcion_TrabajadorPagoEstatus'] == null)? 'Sin percepciones' : $montos['Descripcion_TrabajadorPagoEstatus']; ?></h4>
						</div>
						<div class="col-sm-3">
							<h4><small>Sueldo bruto:</small><br/>$<?php echo ($montos['TotalPercepcion'] == null)? '0.00' : utils::formatMoney($montos['TotalPercepcion']); ?></h4>
						</div>
						<div class="col-sm-3">
							<h4><small>Deducciones:</small><br/>$<?php echo ($montos['TotalDeduccion'] == null)? '0.00' : utils::formatMoney($montos['TotalDeduccion']); ?></h4>
						</div>
						<div class="col-sm-3">
							<h4><small>Sueldo neto:</small><br/>$<?php echo ($montos['Total'] == null)? '0.00' : utils::formatMoney($montos['Total']); ?></h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>

<div class="row mt-3">
	<div class="col-sm-6">

		<div class="card">
			<div class="card-body">
			<h4 class="mb-4">Percepciones</h4>
				<div class="table-responsive">
					<table id="table_content_percepcion" class="table">
					</table>
				</div>
			</div>
		</div>

	</div>

	<div class="col-sm-6">
		<div class="card">
			<div class="card-body">
			<h4 class="mb-4">Deducciones</h4>
				<div class="table-responsive">
					<table id="table_content_deducciones" class="table">
					</table>
				</div>
			</div>
		</div>

	</div>

</div>
