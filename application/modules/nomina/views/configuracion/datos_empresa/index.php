<div class="card mt-4">
	<div class="card-body ">

		<form id="general_form">

            <!-- <div class="row">
                
                <div class="col-sm-6">
                    
                    {grupo_0}

                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        <div class="">
                            {input}
                            <div class="input-group mb-3">
                                <div class="custom-file">
                                    <input type="file" accept='image/*' onchange="Apps.adjuntar_archivo(this);" class="custom-file-input" id="imagen_logo" aria-describedby="imagen_logo" />
                                    <label class="custom-file-label" for="imagen_logo"></label>
                                </div>
                            </div>

                            <small id="msg_{key}" class="form-text text-danger"></small>
                        </div>
                    </div>
                    {/grupo_0}
                </div>
                <div class="col-sm-6">
                    
                    {grupo_1}

                    <div class="form-group" style="display:none;">
                        <label for="{key}" class="">{label}</label>
                        <div class="">
                            {input}
                        </div>
                    </div>

                    <img id="preview_imagen" style="display:block;max-height: 130px;" onerror="this.src='<?php echo base_url('js/nomina/blanco.png'); ?>'" class="img-fluid" src="<?php echo NOMINA_UPLOADS; ?>{value}"  />
                    {/grupo_1}
                </div>
            </div> -->
            <div class="row mt-3">

                <div class="col-sm-6">
                    {grupo_2}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        <div class="">
                            {input}
                            <small id="msg_{key}" class="form-text text-danger"></small>
                        </div>
                    </div>
                    {/grupo_2}
                </div>

                <div class="col-sm-6">
                    {grupo_3}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        <div class="">
                            {input}
                            <small id="msg_{key}" class="form-text text-danger"></small>
                        </div>
                    </div>
                    {/grupo_3}
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-sm-12">
                    {grupo_4}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        <div class="">
                            {input}
                            <small id="msg_{key}" class="form-text text-danger"></small>
                        </div>
                    </div>
                    {/grupo_4}
                </div>

                <div class="col-sm-6">
                    {grupo_5}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        {input}
                        <small id="msg_{key}" class="form-text text-danger"></small>
                    </div>
                    {/grupo_5}
                </div>

                <div class="col-sm-6">
                    {grupo_6}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        {input}
                        <small id="msg_{key}" class="form-text text-danger"></small>
                    </div>
                    {/grupo_6}
                </div>

                <div class="col-sm-6">
                    {grupo_7}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        {input}
                        <small id="msg_{key}" class="form-text text-danger"></small>
                    </div>
                    {/grupo_7}
                </div>
            </div>
            <div class="row mt-3">

                <div class="col-sm-6">
                    {grupo_8}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        {input}
                        <small id="msg_{key}" class="form-text text-danger"></small>
                    </div>
                    {/grupo_8}
                </div>

                <div class="col-sm-6">
                    {grupo_9}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        {input}
                        <small id="msg_{key}" class="form-text text-danger"></small>
                    </div>
                    {/grupo_9}
                </div>

                <div class="col-sm-6">
                    {grupo_10}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        {input}
                        <small id="msg_{key}" class="form-text text-danger"></small>
                    </div>
                    {/grupo_10}
                </div>

                <div class="col-sm-6">
                    {grupo_11}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        {input}
                        <small id="msg_{key}" class="form-text text-danger"></small>
                    </div>
                    {/grupo_11}
                </div>

                <div class="col-sm-6">
                    {grupo_12}
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        {input}
                        <small id="msg_{key}" class="form-text text-danger"></small>
                    </div>
                    {/grupo_12}
                </div>

            </div>

            

		</form>

	</div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group mt-3">
            <div class="row">
                <div class="col-sm-3">&nbsp;</div>
                <div class="col-sm-9">
                    <button onclick="Apps.guardar(this);" type="button" class="btn btn-success col-md-4">Guardar</button>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>