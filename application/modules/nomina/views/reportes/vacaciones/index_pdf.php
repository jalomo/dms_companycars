<style>
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {background-color: #f2f2f2;}
</style>

<br/>
<div style="">
    <table style="" class="" cellpadding="5">
    <?php if(isset($fecha_inicio) && strlen(trim($fecha_inicio)) > 0 ){ ?>
        <tr>
            <th width="150px" bgcolor="">
                Fechas:
            </th>
            <td>
                <?php echo (isset($fecha_inicio))? utils::aFecha($fecha_inicio,true).' al '.utils::aFecha($fecha_fin,true) : '' ?>
            </td>
        </tr>
        <?php } ?>
    </table>
</div>
<br/>