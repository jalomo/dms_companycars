<?php
	echo script_tag('js/nomina/jquery.fileDownload.js');
?>

<script id="tmpl_trabajadores" type="text/template">
	{{#data}}
    <div class="custom-control custom-checkbox">
        <input checked type="checkbox" name="id_trabajador[]" class="custom-control-input" value="{{id}}"
            id="id_trabajador_{{id}}">
        <label class="custom-control-label" for="id_trabajador_{{id}}">{{Clave}} -
            {{Nombre}} {{Apellido_1}} {{Apellido_2}}</label>
    </div>
    {{/data}}
</script>

<div class="row justify-content-md-center">

	<div class="col-sm-12 mb-2">
		<h3 class="card-title text-dark">Horas Extras</h3>
	</div>
	<div class="col-sm-12">

		<div class="card">
			<div class="card-body">

				<form id="form_content">

					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="">Trabajadores</label>
								<div id="listado_trabajadores">
									{trabajadores}
									<div class="custom-control custom-checkbox">
										<input checked type="checkbox" name="id_trabajador[]"
											class="custom-control-input" value="{id}" id="id_trabajador_{id}">
										<label class="custom-control-label" for="id_trabajador_{id}">{Clave} -
											{Nombre} {Apellido_1} {Apellido_2}</label>
									</div>
									{/trabajadores}
								</div>
							</div>
						</div>

						<div class="col-sm-8">
							<div class="form-group">
								<label for="id_puestos">Rango de fechas de disfrute</label>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group row">
											<label for="fecha_inicio" class="col-sm-3 col-form-label">Desde: </label>
											<div class="col-sm-9">
												<input type="date" class="form-control" id="fecha_inicio"
													name="fecha_inicio">
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group row">
											<label for="fecha_fin" class="col-sm-3 col-form-label">Hasta: </label>
											<div class="col-sm-9">
												<input type="date" class="form-control" id="fecha_fin" name="fecha_fin">
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="row mt-4">
						<div class="col-sm-8">&nbsp;</div>
						<div class="col-sm-4">
							<button class="btn btn-secondary" onclick="location.reload();"
								type="button">Restablecer</button>
							<button class="btn btn-success" onclick="Apps.cargarDatos();" type="button">Buscar</button>
						</div>
					</div>

				</form>


			</div>

		</div>
	</div>
</div>

<div class="row justify-content-md-center mt-4">

	<div class="col-sm-12">

		<div class="card">
			<div class="card-body">
				<div class="row mb-1">
					<div class="col-md-8"></div>
					<div class="col-md-4 mb-4" align="right">
						<!-- <a class="btn btn-primary"
							href="<?php echo site_url('nomina/inicio/trabajador/alta') ?>">Exportar a excel</a> -->
						<button class="btn btn-primary" type="button" onclick="Apps.descargar();">Exportar</button>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered dataTable" id="listado" width="100%" cellspacing="0">
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
