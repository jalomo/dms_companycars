<script>
    var  id = "{id}";
    var  id_trabajador = "{id_trabajador}";
</script>

<div class="row">
    <div class="col-sm-12">
        <h3 class="mt-2 mb-3">Alta al hístorico de salarios</h3>
    </div>
</div>
<div class="card">
	<div class="card-body ">

		<form id="general_form">
            <h4 class="card-title text-dark">Datos del trabajador</h4>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="dia" class="col-form-label">Clave</label>
                            <input type="" class="form-control-plaintext"  value="<?php echo $trabajador['Clave']; ?>" >
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="dia" class="col-form-label">Nombre</label>
                        <input type="" class="form-control-plaintext"  value="<?php echo $trabajador['Nombre'].' '.$trabajador['Apellido_1'].' '.$trabajador['Apellido_2']; ?>" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="dia" class="col-form-label">Departamento</label>
                        <input type="" class="form-control-plaintext"  value="<?php echo $trabajador['Descripcion_Departamento']; ?>" >
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="dia" class="col-form-label">Puesto</label>
                        <input type="" class="form-control-plaintext"  value="<?php echo $trabajador['Descripcion_Puesto']; ?>" >
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="dia" class="col-form-label">Fecha de alta</label>
                        <input type="" class="form-control-plaintext"  value="<?php echo utils::aFecha($trabajador['FechaAlta']); ?>" >
                    </div>
                </div>
            </div>

            <h4 class="card-title text-dark mt-5">Salario por hora</h4>
            <div class="row">
                <div class="col-sm-3">&nbsp;</div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="dia" class="col-form-label">Salario por hora</label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="dia" class="col-form-label">Horas por día</label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="dia" class="col-form-label">Día por semana</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="dia" class="col-form-label">Actual</label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input type="" class="form-control-plaintext"  value="$<?php echo utils::formatMoney($salario['SalarioHora']); ?>" >
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input type="" class="form-control-plaintext"  value="<?php echo $salario['HorasDia']; ?>" >
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input type="" class="form-control-plaintext"  value="<?php echo $salario['DiasSemana']; ?>" >
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="dia" class="col-form-label">Nuevo</label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input type="" class="form-control-plaintext" id="SalarioHora" value="$<?php echo utils::formatMoney($salario['SalarioHora']); ?>" >
                        <input type="hidden" class="form-control-plaintext" name="SalarioHora" value="<?php echo $salario['SalarioHora']; ?>" >
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input type="" class="form-control-plaintext" id="HorasDia" value="<?php echo $salario['HorasDia']; ?>" >
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input type="" class="form-control-plaintext"  id="DiasSemana" value="<?php echo $salario['DiasSemana']; ?>" >
                    </div>
                </div>
            </div>

            <h4 class="card-title text-dark mt-5">Salario por día</h4>
            <div class="row">
                <div class="col-sm-3">&nbsp;</div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="dia" class="col-form-label">A partir del</label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="dia" class="col-form-label">Salario por día</label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="dia" class="col-form-label">SDI</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="dia" class="col-form-label">Actual</label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input type="" class="form-control-plaintext"  value="<?php echo utils::aFecha($salario['FechaAplicacion'],true); ?>" >
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input type="" class="form-control-plaintext" value="$<?php echo utils::formatMoney($salario['SalarioDia']); ?>" >
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input type="" class="form-control-plaintext"  value="$<?php echo utils::formatMoney($salario['SDICapturado']); ?>" >
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="dia" class="col-form-label">Nuevo</label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input type="date" name="FechaAplicacion" min="<?php echo date('Y-m-d', strtotime($salario['FechaAplicacion']. ' + 1 days')); ?>" class="form-control"  value="<?php echo date('Y-m-d', strtotime($salario['FechaAplicacion']. ' + 1 days')); ?>" >
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input type="number" step="00.01" class="form-control" onblur="Apps.Recalcular(this);"  name="SalarioDia" value="<?php echo ($salario['SalarioDia']); ?>" >
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <input type="number" step="00.01" class="form-control" name="SDICapturado"  value="<?php echo ($salario['SDICapturado']); ?>" >
                    </div>
                </div>
            </div>

            
            <div class="col-sm-12">
                <div class="form-group row mt-3">
                    <div class="col-sm-3">&nbsp;</div>
                    <div class="col-sm-9">
                        <button onclick="Apps.guardar(this);" type="button" class="btn btn-success col-md-4">Guardar</button>
                    </div>
                </div>
            </div>

		</form>

	</div>
</div>