<script>
    var  id = "{id}";
    var  base_fiscal_tipo = "{base_fiscal_tipo}";
</script>

<div class="card">
	<div class="card-body">
		<form id="general_form">
			<div class="row">
				<div class="col-sm-12">
					<h3 class="mt-4 mb-5">Modificar percepciones por base fiscal</h3>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="">Concepto</label>
						<input type="" class="form-control-plaintext" id=""
							value="<?php echo $registros['Num_BaseFiscalTipo'].' - '.$registros['Descripcion_BaseFiscalTipo']; ?>">
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="">Periodo</label>
						<input type="" class="form-control-plaintext" id=""
							value="<?php echo $registros2['Clave_Calendario'].' - '.$registros2['Descripcion_Calendario']; ?>">
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="id_BaseFiscalMonto">Monto</label>
						<select name="id_BaseFiscalMonto" class="form-control" id="id_BaseFiscalMonto" attr-id="<?php echo $registros['id_BaseFiscalMonto']; ?>">
							{montos}
							<option value="{id}">{Descripcion}</option>
							{/montos}
						</select>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="Formula">Fórmula</label>
						<input type="text" name="Formula" id="Formula" class="form-control" value="<?php echo $registros['Formula']; ?>">
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group">
						<label for="AcumuladoPeriodo">Acumulado periodo</label>
						<input type="number" min="0" step="0.01" name="AcumuladoPeriodo" id="AcumuladoPeriodo" class="form-control" value="<?php echo $registros['AcumuladoPeriodo']; ?>">
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group">
						<label for="AcumuladoAnual">Acumulado anual</label>
						<input type="number" min="0" step="0.01" name="AcumuladoAnual" id="AcumuladoAnual" class="form-control" value="<?php echo $registros['AcumuladoAnual']; ?>">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">&nbsp;</div>
				<div class="col-sm-9">
					<button onclick="Apps.guardar();" type="button" class="btn btn-success col-md-4">Guardar</button>
				</div>
			</div>

		</form>
	</div>
</div>
