@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="formulario_preventa"> <!-- enctype="multipart/form-data"  -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">No. de Orden:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->orden)){print_r($data->orden);} ?>"  id="orden" name="orden" placeholder="1010">
                            <div id="orden_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Usuario que solicita:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->usuario)){print_r($data->usuario);} ?>"  id="usuario" name="usuario" placeholder="ADMIN">
                            <div id="usuario_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="">Fecha de solicitud:</label>
                            <input type="date" class="form-control" min="<?= date('Y-m-d'); ?>" value="<?php if(isset($data->fecha)){print_r($data->fecha);}else {echo date('Y-m-d');} ?>" id="fecha" name="fecha" placeholder="">
                            <div id="fecha_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="">Número de pedido:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->pedido)){print_r($data->pedido);} ?>"  id="pedido" name="pedido" placeholder="00001">
                            <div id="pedido_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Sucursal:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->sucursal)){print_r($data->sucursal);} ?>"  id="sucursal" name="sucursal" placeholder="M2353">
                            <div id="sucursal_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Moneda:</label>
                            <select class="form-control" id="modena" name="modena" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="MXN">MXN (Pesos Mexicanos)</option>
                            </select>
                            <div id="modena_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Forma de pago:</label>
                            <select class="form-control" id="forma_pago" name="forma_pago" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_tipo_pago))
                                    @foreach ($cat_tipo_pago as $pago)
                                        <option value="{{$pago->id}}">[{{$pago->clave}}] {{$pago->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="forma_pago_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Método de pago:</label>
                            <select class="form-control" id="metodo_pago" name="metodo_pago" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="PUE">Pago en una sola exhibición</option>
                                <option value="PPD">Pago en parcialidades o diferido</option>
                            </select>
                            <div id="metodo_pago_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">CFDI:</label>
                            <select class="form-control" id="cfdi_id" name="cfdi_id" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_cfdi))
                                    @foreach ($cat_cfdi as $cfdi)
                                        <option value="{{$cfdi->id}}">[{{$cfdi->clave}}] {{$cfdi->descripcion}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="cfdi_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for=""># Económico:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->economico)){print_r($data->economico);} ?>"  id="economico" name="economico" placeholder="">
                            <div id="economico_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="">Plan de credito (Ford Credit):</label>
                            <select class="form-control" id="plan_credito" name="plan_credito" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="PLAN FORD BLUE">PLAN FORD BLUE</option>
                                <option value="PLAN TRADICIONAL">PLAN TRADICIONAL</option>
                                <option value="PLAN COMERCIAL">PLAN COMERCIAL</option>
                                <option value="PLAN INDEPENDIENTES">PLAN INDEPENDIENTES</option>
                                <option value="PLAN PARA VEHICULOS COMPARTIDOS">PLAN PARA VEHICULOS COMPARTIDOS</option>
                                <option value="PLAN SEMINUEVOS">PLAN SEMINUEVOS</option>
                            </select>
                            <div id="plan_credito_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <br>
                <h4 style="text-align: center;">Datos del cliente</h4>
                <hr>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Número de cliente:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->clave_cliente)){print_r($data->clave_cliente);} ?>"  id="clave_cliente" name="clave_cliente" placeholder="00AS">
                            <div id="clave_cliente_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="">Cliente:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->cliente)){print_r($data->cliente);} ?>"  id="cliente" name="cliente" placeholder="Cliente demo">
                            <div id="cliente_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="">Razón Social:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->razon)){print_r($data->razon);} ?>"  id="razon" name="razon" placeholder="Cliente A SA DE CV">
                            <div id="razon_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">RFC:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->rfc)){print_r($data->rfc);} ?>"  id="rfc" name="rfc" placeholder="XXX09X090XXX">
                            <div id="rfc_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="">Dirección:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->domicilio)){print_r($data->domicilio);} ?>"  id="domicilio" name="domicilio" placeholder="">
                            <div id="domicilio_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Estado:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->edo)){print_r($data->edo);} ?>"  id="edo" name="edo" placeholder="">
                            <div id="edo_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Municipio:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->municipio)){print_r($data->municipio);} ?>"  id="municipio" name="municipio" placeholder="">
                            <div id="municipio_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Colonial:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->col)){print_r($data->col);} ?>"  id="col" name="col" placeholder="">
                            <div id="col_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Código postal:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->cp)){print_r($data->cp);} ?>"  id="cp" name="cp" placeholder="">
                            <div id="cp_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Telefono :</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->tel1)){print_r($data->tel1);} ?>"  id="tel1" name="tel1" placeholder="">
                            <div id="tel1_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Correo Electrónico :</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->email1)){print_r($data->email1);} ?>"  id="email1" name="email1" placeholder="">
                            <div id="email1_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">NOTAS:</label>
                            <textarea class="form-control" cols="4" id="nota" name="nota"><?php if(isset($data->nota)){print_r($data->nota);} ?></textarea>
                            <div id="nota_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <br>
                <h4 style="text-align: center;">Datos de la unidad</h4>
                <hr> 

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Marca:</label>
                            <select class="form-control" id="marca" name="marca" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_marcas))
                                    @foreach ($cat_marcas as $marca)
                                        <option value="{{$marca->id}}">{{$marca->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="marca_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Modelo:</label>
                            <select class="form-control" id="modelo" name="modelo" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_modelo))
                                    @foreach ($cat_modelo as $modelo)
                                        <option value="{{$modelo->id}}">{{$modelo->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="no_identificacion_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Año:</label>
                            <select class="form-control" id="anio" name="anio" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_anio))
                                    @foreach ($cat_anio as $anio)
                                        <option value="{{$anio->id}}">{{$anio->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="anio_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Color:</label>
                            <select class="form-control" id="color" name="color" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_color))
                                    @foreach ($cat_color as $color)
                                        <option value="{{$color->id}}">{{$color->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="color_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Motor:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->motor)){print_r($data->motor);} ?>"  id="motor" name="motor" placeholder="">
                            <div id="motor_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Transmisión:</label>
                            <!--<input type="text" class="form-control" value="<?php if(isset($data->transmision)){print_r($data->transmision);} ?>"  id="transmision" name="transmision" placeholder="">-->
                            <select class="form-control" id="transmision" name="transmision" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="AUTOMÁTICA" <?php if(isset($data->transmision)){ if($data->transmision == "AUTOMÁTICA") echo "selected";} ?> >AUTOMÁTICA </option>
                                <option value="ESTÁNDAR" <?php if(isset($data->transmision)){ if($data->transmision == "ESTÁNDAR") echo "selected";} ?> >ESTÁNDAR </option>
                                <option value="CVT" <?php if(isset($data->transmision)){ if($data->transmision == "CVT") echo "selected";} ?> >CVT </option>
                                <option value="SEMI-AUTOMÁTICA" <?php if(isset($data->transmision)){ if($data->transmision == "SEMI-AUTOMÁTICA") echo "selected";} ?> >SEMI-AUTOMÁTICA </option>
                            </select>
                            <div id="transmision_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Kilometraje:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->kilometraje)){print_r($data->kilometraje);} ?>"  id="kilometraje" name="kilometraje" placeholder="">
                            <div id="kilometraje_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">N° cilindros:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->cilindros)){print_r($data->cilindros);} ?>"  id="cilindros" name="cilindros" placeholder="">
                            <div id="cilindros_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Catálogo:</label>
                            <select class="form-control" id="catalogo" name="catalogo" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                            </select>
                            <div id="catalogo_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for=""># Económico:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->economico)){print_r($data->economico);} ?>"  id="economico" name="economico" placeholder="">
                            <div id="economico_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Capacidad:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->capacidad)){print_r($data->capacidad);} ?>"  id="capacidad" name="capacidad" placeholder="">
                            <div id="capacidad_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">N° de puertas:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->puertas)){print_r($data->puertas);} ?>"  id="puertas" name="puertas" placeholder="">
                            <div id="puertas_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Combustible:</label>
                            <select class="form-control" id="combustible" name="combustible" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="Gasolina">Gasolina</option>
                                <option value="Disel">Disel</option>
                            </select>
                            <div id="combustible_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="">VIN:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->vin)){print_r($data->vin);} ?>"  id="vin" name="vin" placeholder="">
                            <div id="vin_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Serie Corta:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->vincorto)){print_r($data->vincorto);} ?>"  id="vincorto" name="vincorto" placeholder="">
                            <div id="vincorto_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Precio Venta:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->precio_venta)){print_r($data->precio_venta);} ?>"  id="precio_venta" name="precio_venta" placeholder="200,000">
                            <div id="precio_venta_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        <button type="button" id="guardar_formulario" class="btn btn-success">Guardar</button>
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
                        
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection