<div style="overflow-y: scroll;height: 400px;" class="row">
	<div class="col-sm-12">
		<table class="table table-striped table-bordered" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th width="30%">Fecha</th>
					<th>Comentario</th>
					@if($tipo_comentario=='1')
						<th>Fecha notificación</th>
					@endif
				</tr>
			</thead>
			<tbody>
				@if(count($comentarios)>0)
				@foreach($comentarios as $c => $value)
					<tr>
						<td width="30%">{{$value->created_at}}</td>
						<td>{{$value->comentario}}</td>
						@if($tipo_comentario=='1')
							<td>{{$value->fecha_notificacion}}</td>
						@endif
					</tr>
				@endforeach
				@else
				<tr class="text-center">
					<td colspan="3">Aún no se han registrado comentarios... </td>
				</tr>
				@endif
			</tbody>

		</table>
	</div>
</div>