<form action="" id="frm_comentarios">
	<input type="hidden" name="id_venta" id="id_venta" value="{{$id_venta}}">
	<input type="hidden" name="fecha" id="fecha" value="{{$fecha}}">
	<input type="hidden" name="cliente" id="fecha" value="{{$cliente}}">

	@if(!$no_contactar)
	<a href ="{{base_url('ventas_web/generar_cita/0').'/'.$id_venta}}" class="btn btn-sm btn-primary pull-right m-t-n-xs"><strong>Agendar cita</strong></a>
	@endif
	<br>
	<br>
	<div class="row">
		<div class="col-sm-6">
			<label for="">Fecha inicio</label>
			<input type="date" class="form-control" value="{{($fecha)}}" name="cronoFecha" id="cronoFecha">
            <span class="error error_cronoFecha"></span>
		</div>
		<div class="col-sm-6"> 
			<label for="">Hora</label>
			<input type="time" class="form-control" value="" name="cronoHora">
	  	</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<strong>No contactar cliente</strong>
			<input type="checkbox" name="no_contactar" id="no_contactar">
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div style="margin-top: 10px" class="alert alert-warning alert-dismissible" role="alert">
				<strong>Al marcar el campo de "No contactar cliente" automáticamente se quitará de la lista para evitar contactar al cliente</strong>.
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<label>Comentario</label>
			{{$input_comentario}}
			<span class="error error_comentario"></span>
		</div>
	</div>
</form>
<script>
	var fecha_actual = "{{$fecha}}";
	var fecha_fin = "{{$fecha_fin}}";
    $("#cronoFecha").val("");
    $("#cronoFecha").val("{{date_eng2esp_1($fecha)}}");
</script>