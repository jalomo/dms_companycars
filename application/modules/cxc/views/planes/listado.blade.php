@layout('tema_luna/layout')
@section('contenido')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row mb-3">
        <div class="col-md-8"></div>
        <div class="col-md-4" align="right">
            <a class="btn btn-primary" href="<?php echo base_url('cxp/PlanesCredito/crear') ?>">Registrar plan</a>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-md-4">
            <label for="">Tipo Persona:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="far fa-user"></i>
                    </div>
                </div>

                <select class="form-control" id="persona" >
                    <option value="">Selecionar ...</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <label for="">Plazo:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="">
                        <i class="fas fa-ellipsis-v"></i>
                    </div>
                </div>

                <select class="form-control" id="plazo" >
                    <option value="">Selecionar ...</option>
                </select>
            </div>
        </div>
        <div class="col-md-4" align="right">
            <a class="btn btn-primary" href="" style="margin-top: 30px;">Buscar</a>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_ventas" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tipo</th>
                            <th>Plazos</th>
                            <th>Enganche</th>
                            <th>Comisión por apertura</th>
                            <th>Tipo de persona</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($data))
                            @foreach ($data as $item)
                            <tr>
                                <td>#</td>
                                <td>{{$item->tipo}}</td>
                                <td>{{$item->plazos}}</td>
                                <td>{{$item->enganches}}</td>
                                <td>{{$item->comision}}</td>
                                <td>{{$item->persona}}</td>
                                <td>
                                    <a href="#" class="btn btn-info" type="button" title="Ver plan">
                                        <i class="far fa-eye"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="#" class="btn btn-success" type="button" title="Simular">
                                        <i class="fas fa-calculator"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>1</td>
                                <td>PLAN FORD BLUE</td>
                                <td>DE 12 - 72 MESES</td>
                                <td>DESDE 10%</td>
                                <td>$0</td>
                                <td>FISICA/MORAL</td>
                                <td>
                                    <a href="{{ base_url('cxp/PlanesCredito/editar/1') }}" class="btn btn-info" type="button" title="Ver plan">
                                        <i class="far fa-eye"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ base_url('cxp/PlanesCredito/simular/1') }}" class="btn btn-success" type="button" title="Simular">
                                        <i class="fas fa-calculator"></i>
                                    </a>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Tipo</th>
                            <th>Plazos</th>
                            <th>Enganche</th>
                            <th>Comisión por apertura</th>
                            <th>Tipo de persona</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

@endsection