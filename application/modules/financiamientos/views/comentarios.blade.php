<form action="" id="frm_comentarios">
    <input type="hidden" name="id_financiamiento" id="id_financiamiento" value="{{ $id_financiamiento }}">
    <div class="row">
        <div class="col-sm-12">
            <label for="">Tipo de comentario</label> <br>
            <input class="js_tipo_comentario" type="radio" name="tipo_comentario" id="tipo1" value="1" checked> <label style="margin-right:10px;"
                for="tipo1">Comentario general</label>
            <input class="js_tipo_comentario" type="radio" name="tipo_comentario" id="tipo2" value="2"> <label style="margin-right:10px;"
                for="tipo2">Comentario medición de tiempo</label>
        </div>
    </div>
    <div class="row estatus_general">
        <div class="col-sm-12">
            <label for="">Estatus</label>
            {{ $drop_estatus }}
            <span class="error error_id_estatus"></span>
        </div>
    </div>
    <div class="row estatus_medicion ocultar">
        <div class="col-sm-12">
            <label for="">Estatus medición de tiempo</label>
            {{ $estatus_piso }}
            <span class="error error_id_estatus_piso"></span>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-12">
            <label>Comentario</label>
            {{ $input_comentario }}
            <span class="error error_comentario"></span>
        </div>
    </div>
</form>

<script>
	$(".js_tipo_comentario").on('click',function(){
		var valor = $(this).val();
		if(valor == 1){
			$(".estatus_medicion").addClass('ocultar');
			$(".estatus_medicion").removeClass('visualizar');
			$(".estatus_general").removeClass('ocultar');
			$(".estatus_general").addClass('visualizar');
		}else{
			$(".estatus_medicion").removeClass('ocultar');
			$(".estatus_medicion").addClass('visualizar');
			$(".estatus_general").addClass('ocultar');
			$(".estatus_general").removeClass('visualizar');
		}
	})
</script>